<?php
define('WP_USE_THEMES', false);
require('./wp-load.php');

$dsn = 'sqlsrv:Server=82.166.92.197;Database=lando07';
$user = 'sa';
$password = 'wizsoft';


function product_exists($ItemKey)
{
    $q = array('post_type' => 'product', 'post_status'   => 'publish',
        'meta_query' => array(
            array(
                'key'     => 'ItemKey',
                'value'   => $ItemKey,
                'compare' => '=',
            ),
        ),
    );
    $prod = new WP_Query($q);
    if ($prod->have_posts()) {
        $prod->the_post();
        return get_the_ID();
    }
    return false;
}

try {
    $dbh = new PDO($dsn, $user, $password);
    set_time_limit(0);
  // $stmt = $dbh->query("SELECT * FROM Varieties");
  // $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
  // var_dump($res)    ;

  // echo "************\n"  ;
    $debug =0;
    $amount = (isset($_GET['amount'])) ? intval($_GET['amount']) : 100;
    $offset = (isset($_GET['offset'])) ? intval($_GET['offset']) : 0;
    if (isset($_GET['debug'])) {
        $debug=1;
    }

    $next = $amount+$offset;

    $q = "WITH Seq AS ( SELECT Id,ItemKey,ItemName,SortGroup,Filter,Price,SalesUnit,BarCode,ExPic, ROW_NUMBER() OVER(ORDER BY Id ASC) AS 'RowNumber' FROM Items ) SELECT Id,ItemKey,ItemName,SortGroup,Filter,Price,SalesUnit,BarCode,ExPic FROM Seq WHERE RowNumber BETWEEN $offset AND $next";
    $stmt = $dbh->query($q);
    $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $i = 0;
    foreach ($res as $p) {
	var_dump($p);
        if ($p['SalesUnit'] != 'NULL') {
            $unit = $p['SalesUnit'];
        } else {
            $unit = 1;
        }
        if (empty($p['ItemName'])) {
            $i++;
            continue;
        }

        $post_data = array(
            'post_title'    => wp_strip_all_tags($p['ItemName']),
            'post_content'  =>  $p['ItemName'],
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type' => 'product',
            'meta_input'   => array(
                'serial' => $p['BarCode'],
                'short_desc' => $p['ItemName'],
                'price_per_piece' => $p['Price'],
                'package_amount'  => $unit,
                'ItemKey' => $p['ItemKey'],
                'SortGroup' => $p['SortGroup']
            ),
        );
        if ($debug) {
            var_dump($p);
        }
        $prodKey = product_exists($p['ItemKey']);
        if ($prodKey !== false) {
            $post_data['ID'] = $prodKey;
        }
        $pid = wp_insert_post($post_data);
        if (!is_wp_error($pid)) {
            $i++;
            $cats = array (1=>'פרחים', 2=>'ורדים', 3=>'ירוקים', 4=>'עציצים', 5=>'אביזרים');
            if ($p['SortGroup'] > 5 || $p['SortGroup'] < 1 || empty($p['SortGroup'])) {
                continue;
            }
            $catname = $cats[$p['SortGroup']];
            $catId = get_term_by('name', $catname, 'product_cat');
            $ret = wp_set_post_terms($pid, $catId->term_id, 'product_cat');
            if ($debug) {
                echo 'Created item '.$p['ItemName']." in category ".$catname.' id '.$pid.' ret '.$ret.'<br/>';
                var_dump($ret);
                var_dump($catId->term_id);
            }
        }
    }
    echo json_encode(array('success' =>true, 'amount' => $i, 'newOffset' => $offset+$i));
    exit;
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
