<html>
	<head>
		<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	</head>
	<body>
		<h1>Starting buki product import</h1>
		<form id='import_form' >
			Start index:
			<input type='text' name='offset' id='offset' value='0' />
			Import amount:
			<input type='text' name='amount' id='amount' value='100' />
			<input type='submit' value='send' />
		</form>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('#import_form').submit(function(event) {
					event.preventDefault();
					jQuery.ajax({
					 	method: "GET",
					 	dataType:'json',
					 	url: "import_products.php",
					 	data: {
					 		offset: $('#offset').val(),
					 		amount: $('#amount').val() 
					 	}
					})
			  		.done(function( msg ) {
			    		if (msg.success == true) {
			    			if (msg.amount >= $('#amount').val()) {
			    				$('#offset').val(msg.newOffset);
			    				$('#import_form').submit();
			    			}
			    		}
			  		});
			  	});
			});
		</script>
	</body>
</html>