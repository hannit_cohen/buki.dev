<?php
define('WP_USE_THEMES', false);
require('./wp-load.php');
$today = strtotime('00:00:00');
$yesterday = strtotime('-1 day', $today);

$new_orders = new WP_Query(array('post_type' => 'buki_order', 'posts_per_page' => 1,/* 'date_query' => array(array(
            'after' => '1 day ago', 'inclusive' => true,
        ))*/));
$fp = fopen('IMOVEIN.DOC', 'w');
if (!$fp) {
	write_log('Error opening data file');
	die();
}

echo "Opened FP succesfully<br/>";
if ($new_orders->have_posts()): 
	echo "Got total ".$new_orders->found_posts." <br/>";
	while ($new_orders->have_posts()): $new_orders->the_post();
		$rec = array();
		$user = get_field('user');
		if (!is_object($user) && !is_array($user)) {
			$user = get_user_by( 'ID', $user);	
		}
		if (is_object($user))
			$uid = $user->ID;
		else
			$uid = $user['ID'];
		$client_id = get_field('AccountKey', 'user_'.$uid);
		
		$rec['AccountKey'] = str_pad($client_id, 15, " ", STR_PAD_RIGHT);
		$rec['OrderId'] = str_pad(get_the_ID(), 9, " ", STR_PAD_RIGHT);
		// $rec .= '|';
		$rec['DocType'] = '30';
		// $rec .= '|';
		// var_dump($rec);
		$fname = get_field('fanme');
		$lname = get_field('lname');
		$client_name = $fname.' '.$lname;
		$client_name = str_pad($client_name, 50, " ", STR_PAD_RIGHT);
		echo strlen($client_name);
		// echo $client_name.'<br/>';
		$rec['ClinetName'] = $client_name;
		// var_dump($rec);
		// $rec .= '|';
		$address = get_field('delivery_address');
		$rec['Address'] = str_pad($address, 50, " ", STR_PAD_RIGHT);
		// $rec .= '|';
		$city = get_field('city', 'user_'.$uid);
		$rec['City'] = str_pad($city, 50, " ", STR_PAD_RIGHT);
		// $rec .= '|';
		$items = get_field('products');
		foreach ($items as $item) {
			$item_rec = $rec;
			$ItemKey = get_field('ItemKey', $item['product']->ID);
			$item_rec['ItemKey'] = str_pad($ItemKey, 20, " ", STR_PAD_RIGHT);
			// $item_rec .= '|';
			$item_rec['Amount'] = str_pad(intval($item['amount']), 9, " ", STR_PAD_RIGHT);
			// $item_rec .= '|';
			$title = str_pad(get_the_title( $item['product']->ID), 100, " ", STR_PAD_RIGHT);
			$item_rec['ProdName'] = $title;
			// $item_rec .= '|';
			$phone = get_field('phone');
			$item_rec['Phone'] = str_pad($phone, 30, " ", STR_PAD_RIGHT);
			// $item_rec .= '|';
			if (is_object($user))
				$email = $user->user_email;
			else
				$email = $user['user_email'];
			$item_rec['Email'] = str_pad($email, 50, " ", STR_PAD_RIGHT);
			// $item_rec .= '|';
			var_dump($item_rec);
			$item_rec = iconv("UTF-8", "windows-1255", implode('|', $item_rec));
			// var_dump($item_rec);
			$res = fwrite($fp, $item_rec."\n");
		}
	endwhile;
else:
	var_dump($new_orders);
endif;

fclose($fp);
