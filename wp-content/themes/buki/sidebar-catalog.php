 <form action='<?php the_permalink();?>' method="get" id='filter_form'>
	<div class='sidebar_filter'>
		<p class='clearfix'>
			<span class='alignright'><strong>סנן לפי:</strong></span>
			<span class='alignleft'><strong><a href='<?php the_permalink();?>' id='cleanall'>נקה הכל</a></strong></span>
		</p>
	</div>
	<div class='sidebar_filter favorites_filter'>
		<?php $favs = get_favorites_arr();?>
		<p><strong>מועדפים (<?php echo count($favs);?>)</strong></p>
		<input type='checkbox' name='favorites' id='favorites' class='checkbox'><label for='favorites'> הצג מוצרים מועדפים</label><br/>
	</div>
	<div class='sidebar_filter featured_filter'>
		<?php $featured = get_featured_count();?>
		<p><strong>מקודמים (<?php echo $featured;?>)</strong></p>
		<input type='checkbox' name='featured' id='featured' class='checkbox'><label for='featured'> הצג מוצרים מקודמים</label><br/>
	</div>
	<div class='sidebar_filter'>
		<?php 
		$cats = get_terms('product_cat', array('parent' => 0)); ?>
		<p class='clearfix'>
			<span class='alignright'><strong>קטגוריה (<?php echo count($cats);?>):</strong></span>
			<span class='alignleft'><strong><a href='' class='clean_filter'>נקה</a></strong></span>
		</p>
		<?php
		foreach ($cats as $cat) {
			echo "<input type='checkbox' class='checkbox' name='product_cat' value='".$cat->term_id."' id='product_cat_".$cat->term_id."'><label for='product_cat_".$cat->term_id."'>".$cat->name." (".$cat->count.")</label><br/>";
		} ?>
	</div>

	<div class='sidebar_filter'>
		<?php 
		$cats = get_terms('product_color', array('parent' => 0)); ?>
		<p class='clearfix'>
			<span class='alignright'><strong>צבע (<?php echo count($cats);?>):</strong></span>
			<span class='alignleft'><strong><a href='' class='clean_filter'>נקה</a></strong></span>
		</p>
		<?php
		foreach ($cats as $cat) {
			echo "<input type='checkbox' class='checkbox' name='product_color' value='".$cat->term_id."' id='product_color_".$cat->term_id."'><label for='product_color_".$cat->term_id."'>".$cat->name."</label><br/>";
		} ?>
	</div>
	<div class='sidebar_filter'>
		<?php 
		$cats = get_terms('product_season', array('parent' => 0, 'orderby' => 'term_id')); ?>
		<p class='clearfix'>
			<span class='alignright'><strong>עונה (<?php echo count($cats);?>):</strong></span>
			<span class='alignleft'><strong><a href='' class='clean_filter'>נקה</a></strong></span>
		</p>
		<p class='season_filter'>
			<?php
			for ($i=1; $i<=12; $i++) {
				echo "<input type='checkbox' name='product_season[".$i."]' value='".$i."' id='product_season_".$i."'><label for='product_season_".$i."'>".$i."</label>";
			} ?>
		</p>
	</div>

	<div class='sidebar_filter'>
		<?php 
		$cats = get_terms('product_open', array('parent' => 0)); ?>
		<p class='clearfix'>
			<span class='alignright'><strong>מצב פתיחה (<?php echo count($cats);?>):</strong></span>
			<span class='alignleft'><strong><a href='' class='clean_filter'>נקה</a></strong></span>
		</p>
		<?php
		foreach ($cats as $cat) {
			echo "<input type='checkbox' class='checkbox' name='product_open' value='".$cat->term_id."' id='product_open_".$cat->term_id."'><label for='product_open_".$cat->term_id."'>".$cat->name."</label><br/>";
		} ?>
	</div>

	<div class='sidebar_filter'>
		<?php 
		$cats = get_terms('product_length', array('parent' => 0)); ?>
		<p class='clearfix'>
			<span class='alignright'><strong>אורך (<?php echo count($cats);?>):</strong></span>
			<span class='alignleft'><strong><a href='' class='clean_filter'>נקה</a></strong></span>
		</p>
		<?php
		foreach ($cats as $cat) {
			echo "<input type='checkbox' class='checkbox' name='product_length' value='".$cat->term_id."' id='product_length_".$cat->term_id."'><label for='product_length_".$cat->term_id."'>".$cat->name."</label><br/>";
		} ?>
	</div>
	
	<input type="submit" name="form_submit" value='סנן' class='buki-btn yellowbg white' />
</form>