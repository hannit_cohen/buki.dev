<?php
get_header('full');
$data = get_order_data(get_the_id());
?>

<article>
	<div class="container">
		<div class="row private_area_title">
			<div class="col-md-9">
				<h1 class='bb f-60'>הזמנה מספר <?php the_id();?></h1>
			</div>
			<div class="col-md-3 text-left">
				<?php
				if (is_user_logged_in()) {
					$user = wp_get_current_user();
					if ( in_array( 'subscriber', (array) $user->roles ) ) { ?>
						<a href="" class="buki-btn yellowbg" id='duplicate_single_order' data-id='<?php the_id(); ?>'>
							<i class="fa fa-files-o" aria-hidden="true"></i> שכפל הזמנה 
						</a>
					<?php }
				} ?>
			</div>
		</div>
		<div class="order_summery">
			<div class="row">
				<div class="col-md-8">
					<p class="f-30"><?php echo $data['fname'].' '.$data['lname'];?></p>
					<p class='f-30'><strong><?php the_field('estimated_total');?></strong></p>
				</div>
				<div class="col-md-2 date-col">
					<span>תאריך הזמנה</span>
					<span><?php echo get_the_date('d/m/Y' ); ?></span>
				</div>
				<div class="col-md-2 date-col">
					<span>תאריך משלוח</span>
					<span><?php the_field('supply_date');?></span>
				</div>
			</div>
			<div class="row" id='order_data'>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-15">
							<p>שם המזמין</p>
							<span><?php echo $data['fname'].' '.$data['lname'];?></span>
							<span><?php echo $data['phone'];?></span>
						</div>
						<div class="col-md-15">
							<p>איש קשר</p>
							<span><?php echo $data['contact_fname'].' '.$data['contact_lname'];?></span>
							<span><?php echo $data['contact_phone'];?></span>
						</div>
						<div class="col-md-15">
							<p>כתובת למשלוח</p>
							<span><?php echo $data['delivery_address'];?></span>
						</div>
						<div class="col-md-15">
							<p>סה"כ פרחים</p>
							<span><?php the_field('flours_amount');?></span>
						</div>
						<div class="col-md-15">
							<p>סטטוס</p>
							<span><?php the_field('status');?></span>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<p>הערות</p>
					<p><?php the_field('comments');?></p>
				</div>
			</div>
		</div>
		<?php
		$products = get_field('products');
		if (!empty($products)) { ?>
		<div id="products">
			<table id='products_table'>
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($products as $row) {
					$prod_id = $row['product']->ID;
					$package_amount = get_field('package_amount', $prod_id);
					$price_per_piece = get_field('price_per_piece', $prod_id);
					echo '<tr>';
					echo '<td width="100">';
					echo wp_get_attachment_image(get_post_thumbnail_id($prod_id ), 'thumbnail');
					echo '</td><td width="35%">';
					echo "<span class='prod_title'>".$row['product']->post_title.'</span>';
					echo '<span class"label">קוד '.get_field('serial', $prod_id).'</span>';
					echo '</td><td>';
					echo ' '.$row['color'];
					echo '</td><td>';
					echo '<span class="label">מצב פתיחה</span> '.$row['open'];
					echo '</td><td>';
					echo '<span class="label">כמות לחבילה</span> '.$package_amount;
					echo '</td><td>';
					echo '<span class="label">מחיר ליח’ </span> '.$price_per_piece;
					echo '</td><td>';
					echo '<span class="label">כמות </span> '.$row['amount'];
					echo '</td><td class="text-left">';
					echo '<span class="label">מחיר </span> '.$price_per_piece*$row['amount'];
					echo '</td></tr>';
				} ?>
				</tbody>
			</table>
		</div>
		<?php } ?>
	</div>
</article>
<?php get_footer(); ?>