<?php

get_header('full'); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<article>
			<div class='arthead'>
				<?php the_post_thumbnail('full' ); ?>
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="part1">
				<div class="container">
					<div class="row boxes">
						<div class="col-md-12">
							<h2 class='text-center'><?php the_field('top_row_title');?></h2>
						</div>
						<div class="col-md-6"><?php the_field('top_row_right');?></div>
						<div class="col-md-6"><?php the_field('top_row_left');?></div>
					</div>
				</div>
			</div>
			<div class="part2">
				<div class="row boxes">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6 reddish">
								<?php the_field('second_text_box')?>
							</div>
							<div class="col-md-6 greyish">
								<?php the_field('third_text_box')?>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<?php
						$id = get_field('side-image');
						echo wp_get_attachment_image( $id, 'full');
						?>
					</div>
				</div>
			</div>
		</article>


	<?php endwhile; // end of the loop. ?>

<?php get_footer('full'); ?>
