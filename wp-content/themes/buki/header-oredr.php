<?php get_template_part( 'parts/base-header');?>
<div class="whitebg">
	<div id="main-filter" class="filter-container">
		<form action="" method='post'>
			<?php 
			$selected = (isset($_POST['product_cat'])) ? $_POST['product_cat'] : '';
			wp_dropdown_categories(array('taxonomy' =>'product_cat', 'show_option_all' => 'בחר קטגוריה', 'class' => 'category', 'depth' => 1, 'hierarchical' => true)); ?>
			<select class='subcat'><option value=''>בחר תת קטגוריה</option></select>
			<input type="text" name="" placeholder="חיפוש חופשי">
			<button type="submit" class="buki-btn yellowbg white">
			    חיפוש
			    <i class="fa fa-search" aria-hidden="true"></i>
			</button>
		</form>
	</div>
</div>
<div class="main-content">
