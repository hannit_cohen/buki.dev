<?php
/**
 * _tk functions and definitions
 *
 * @package _tk
 */
 
 /**
  * Store the theme's directory path and uri in constants
  */
 define('THEME_DIR_PATH', get_template_directory());
 define('THEME_DIR_URI', get_template_directory_uri());

include "lib/products.php";
include "lib/modals.php";
include "lib/orders.php";

if ( ! current_user_can( 'manage_options' ) ) {
    show_admin_bar( false );
}

add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}
/**
 * Set the content width based  the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
    $content_width = 750; /* pixels */

if ( ! function_exists( '_tk_setup' ) ) :
/**
 * Set up theme defaults and register support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function _tk_setup() {
    global $cap, $content_width;

    // Add html5 behavior for some theme elements
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

    // This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style();

    /**
     * Add default posts and comments RSS feed links to head
    */
    add_theme_support( 'automatic-feed-links' );

    /**
     * Enable support for Post Thumbnails on posts and pages
     *
     * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
    */
    add_theme_support( 'post-thumbnails' );
    add_image_size('product_thumb', 215, 215, true );
    add_image_size('product_small_thumb', 80, 80, true );
    add_image_size('product_slider', 248, 360, true );
    /**
     * Enable support for Post Formats
    */
    add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

    /**
     * Setup the WordPress core custom background feature.
    */
    add_theme_support( 'custom-background', apply_filters( '_tk_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
        ) ) );
    
    /**
     * Make theme available for translation
     * Translations can be filed in the /languages/ directory
     * If you're building a theme based on _tk, use a find and replace
     * to change '_tk' to the name of your theme in all the template files
    */
    load_theme_textdomain( '_tk', THEME_DIR_PATH . '/languages' );

    /**
     * This theme uses wp_nav_menu() in one location.
    */
    register_nav_menus( array(
        'primary'  => __( 'Header bottom menu', '_tk' ),
        'sitemap'  => __( 'footer sitemap menu', '_tk' ),
        'private_area' => 'Private user area,'
        ) );

}
endif; // _tk_setup
add_action( 'after_setup_theme', '_tk_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function _tk_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Sidebar', '_tk' ),
        'id'            => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
        ) );
}
add_action( 'widgets_init', '_tk_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function _tk_scripts() {
    wp_enqueue_style( '_tk-bootstrap-wp', THEME_DIR_URI . '/includes/css/bootstrap-wp.css' );
    wp_enqueue_style( '_tk-bootstrap', THEME_DIR_URI . '/includes/css/bootstrap.min.css' );
    // wp_enqueue_style( '_tk-bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
    // wp_enqueue_style('rtl-bootstrap', THEME_DIR_URI . '/includes/css/bootstrap-rtl.min.css');
    wp_enqueue_style('rtl-bootstrap', THEME_DIR_URI . '/includes/css/bootstrap-rtl.min.css');
    
    wp_enqueue_style( '_tk-font-awesome', THEME_DIR_URI . '/includes/css/font-awesome.min.css', false, '4.1.0' );
    wp_enqueue_style( '_tk-style', get_stylesheet_uri(), false, filemtime(THEME_DIR_PATH.'/style.css') );
    wp_enqueue_script('_tk-bootstrapjs', THEME_DIR_URI . '/includes/js/bootstrap.min.js', array('jquery'), filemtime(THEME_DIR_PATH.'/includes/js/bootstrap.min.js'), true );
    // wp_enqueue_script('_tk-bootstrapjs', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '3.3.7', true);

    wp_enqueue_script( '_tk-bootstrapwp', THEME_DIR_URI . '/includes/js/bootstrap-wp.js', array('jquery'),  filemtime(THEME_DIR_PATH . '/includes/js/bootstrap-wp.js'), true);
    wp_enqueue_script( '_tk-skip-link-focus-fix', THEME_DIR_URI . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );
    wp_enqueue_script( 'buki', THEME_DIR_URI . '/includes/js/buki.js', array(), filemtime(THEME_DIR_PATH.'/includes/js/buki.js'), true );
    wp_localize_script( 'buki', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'user_id' => get_current_user_id() ) );

    if (is_front_page()) {
        // wp_enqueue_style('slick', THEME_DIR_URI.'/includes/css/slick.css' );
        // wp_enqueue_script('slick', THEME_DIR_URI.'/includes/js/slick.min.js', array(), false, true );

        wp_enqueue_style('slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css' );
        wp_enqueue_script('slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array(), false, true );
        wp_enqueue_style('colorbox', THEME_DIR_URI.'/includes/css/colorbox.css', false,  filemtime(THEME_DIR_PATH.'/includes/css/colorbox.css'));
        wp_enqueue_script('colorbox', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.6.4/jquery.colorbox-min.js', array('jquery'), '1.6.4', true);
    }
    
    // if (is_page_template('page-templates/catalog.php' )) {
    // //   wp_enqueue_script('isotope', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.2/isotope.pkgd.min.js', array('jquery'), '2.2.2', true);
    //  wp_enqueue_script('catalog', THEME_DIR_URI.'/includes/js/catalog.js', array(), filemtime(THEME_DIR_PATH.'/includes/js/catalog.js'), true );
    // }
    wp_enqueue_style( 'datatables', 'https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css' );
    wp_enqueue_script('datatables', 'https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js', array('jquery'), '1.10.15', true);
    wp_enqueue_script('datatables-bs', 'https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js', array('jquery'), '1.10.15', true);
    wp_enqueue_style('datatables-responsive', 'https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css');
    wp_enqueue_script('datatables-responsive', 'https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js', array('jquery'), '2.2.1', true);

    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-datepicker');

}
add_action( 'wp_enqueue_scripts', '_tk_scripts' );


function ja_global_enqueues() {
    wp_enqueue_style(
        'jquery-auto-complete',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css',
        array(),
        '1.0.7'
    );
    wp_enqueue_script(
        'jquery-auto-complete',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.js',
        array( 'jquery' ),
        '1.0.7',
        true
    );
}
add_action('wp_enqueue_scripts', 'ja_global_enqueues');


function ja_ajax_search()
{
    $query = array(
        'post_type'     => array( 'product' ),
        'post_status'   => 'publish',
        'nopaging'      => true,
        'posts_per_page'=> -1,
        's'             => stripslashes($_POST['search']),
    );
    if (!empty($_POST['cat_id'])) {
        $query['tax_query'] = array(
                array('taxonomy' => 'product_cat', 'field' => 'term_id', 'terms' => intval($_POST['cat_id']))
            );
    }
    $results = new WP_Query($query);
    $items = array();
    if (!empty($results->posts)) {
        foreach ($results->posts as $result) {
            $items[] = array($result->post_title, $result->ID);
        }
    }
    wp_send_json_success($items);
}
add_action('wp_ajax_search_site', 'ja_ajax_search');
add_action('wp_ajax_nopriv_search_site', 'ja_ajax_search');

/**
 * Implement the Custom Header feature.
 */
require THEME_DIR_PATH . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require THEME_DIR_PATH . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require THEME_DIR_PATH . '/includes/extras.php';

/**
 * Customizer additions.
 */
require THEME_DIR_PATH . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require THEME_DIR_PATH . '/includes/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require THEME_DIR_PATH . '/includes/bootstrap-wp-navwalker.php';

/**
 * Adds WooCommerce support
 */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


$result = add_role(
    'buki_mgr',
    __( 'Manager' ),
    array(
        'read'         => true,
        'edit_posts'   => true,
        'delete_posts' => false,
    )
);

function buki_bulk_create_users() {
    for ($i - 0; $i < 40; $i++) {
        $pass = wp_generate_password( 8);
        $userdata = array(
            'user_login'  =>  'aaa'.$i,
            'user_pass'   =>  $pass,
            'user_email' => 'aaa'.$i.'@buki.com',
            'first_name' => 'משתלת',
            'last_name' => 'שלומית'
        );
        $uid = wp_insert_user($userdata);
        if (!is_wp_error($uid )) {
            update_field('user_pass', $pass, 'user_'.$uid);
            update_field('user_phone1', '0546280490', 'user_'.$uid);
            update_field('user_phone2', '0546280490', 'user_'.$uid);
            update_field('address', 'ארלוזרוב '.$i, 'user_'.$uid);
            update_field('city', 'גבעתיים '.$i, 'user_'.$uid);
            update_field('zip', '700 '.$i, 'user_'.$uid);
            update_field('contact_fname', 'איש קשר ', 'user_'.$uid);
            update_field('contact_lname', $i, 'user_'.$uid);

        }
    }
}
// buki_bulk_create_users();

function my_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        //check for admins
        if ( in_array( 'administrator', $user->roles ) ) {
            // redirect them to the default place
            return $redirect_to;
        } elseif (in_array( 'buki_mgr', $user->roles ) ) {
            $page = get_page_by_title("פאנל ניהול");
            return get_permalink($page->ID);
        } else {
            return home_url();
        }
    } else {
        return $redirect_to;
    }
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );