<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _tk
 */
$catalog = get_page_by_title('קטלוג');
$catalog_url = get_permalink($catalog->ID);
get_header(); ?>


	<div class="row boxes">
		<div class="col-md-4">
			<div class="top-cat" id='cat1'>
				<div class="hover">
					<h2>פרחים</h2>
					<a href='<?php echo $catalog_url; ?>' class='buki-btn yellowbg'>כניסה לקטלוג ></a>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="top-cat" id='cat2'>
				<div class="hover">
					<h2>עציצים</h2>
					<a href='<?php echo $catalog_url; ?>' class='buki-btn yellowbg'>כניסה לקטלוג ></a>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="top-cat" id='cat3'>
				<div class="hover">
					<h2>ורדים</h2>
					<a href='<?php echo $catalog_url; ?>' class='buki-btn yellowbg'>כניסה לקטלוג ></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row boxes">
		<div class="col-md-4">
			<div class="slider tall">
				<?php hp_featured_products(); ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="top-cat" id='cat4'>
				<div class="hover">
					<h2>אביזרים</h2>
					<a hef='' class='buki-btn yellowbg'>כניסה לקטלוג ></a>
				</div>
			</div>
			<div class="top-cat" id='cat5'>
				<div class="hover">
					<h2>ירוקים</h2>
					<a hef='' class='buki-btn yellowbg'>כניסה לקטלוג ></a>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="bottom-contact text-center tall">
				<h2>השאירו פרטים ונחזור אליכם בהקדם!</h2>
				<?php echo do_shortcode('[contact-form-7 id="17" title="Contact form 1"]'); ?>
			</div>
		</div>
	</div>

	<div class="row secline">
		<div class="col-md-8 clearfix">
			<img class="img1" src="<?php echo get_stylesheet_directory_uri();?>/images/img-about-2.png" />
			<img class='img2' src="<?php echo get_stylesheet_directory_uri();?>/images/img-about-1.jpg" />
		</div>
		<div class="col-md-4">
			<div class="company-about">
				<?php $about = get_page_by_title("הסיפור מאחורי החברה");?>
				<h1>הסיפור מאחורי החברה</h1>
				<h3>איך הכל התחיל</h3>
				<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.  קורוס בליקרה, נונסטי קלובר ברי</p>
				<a href='<?php echo get_permalink($about->ID);?>' class='yellowbg buki-btn'>קרא עוד ></a>
			</div>

		</div>
	</div>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.slider').slick({
				rtl:true,
				infinite: true,
			  	speed: 500,
			  	fade: true,
			  	cssEase: 'linear'
			});
		});
	</script>

<?php get_footer(); ?>
