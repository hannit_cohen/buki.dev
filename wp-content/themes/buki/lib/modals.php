<?php
function login_popup_form() {
	?>
	<div id="login-box">
		<p class='f-18'>התחבר</p>
		<?php wp_login_form(array('remember' => false)); ?>
		<p class='text-center contact'>לא רשום ? <a href='<?php echo get_permalink(9);?>'>צור קשר</a></p>
	</div>
	<?php
}
add_action( 'wp_footer', 'login_popup_form' );

function user_menu_popup_form() {
	if (is_user_logged_in()) {
		$user = wp_get_current_user();
		if (in_array('buki_mgr', $user->roles))
			buki_mgr_menu();
		else
			buki_standard_user_menu();
	}
}
add_action( 'wp_footer', 'user_menu_popup_form' );

function buki_standard_user_menu() {
	?>
	<div id="user_menu">
	<?php 
	$amounts = buki_get_user_amounts(); ?>
		<ul>
			<li class='yellowbg buki-btn'>
				<a href="<?php $page =get_page_by_title('הזמנה חדשה'); echo get_permalink($page->ID);?>">יצירת הזמנה חדשה</a>
			</li>
			<li><a href="<?php $page =get_page_by_title('הזמנות בהמתנה'); echo get_permalink($page->ID);?>">
			הזמנות ממתינות
			<?php if (!empty($amounts['waiting']))
				echo '('.$amounts['waiting'].')';?>
			</a></li>
			<li><a href="<?php $page =get_page_by_title('פרטי החשבון שלי'); echo get_permalink($page->ID);?>">פרטי החשבון שלי</a></li>
			<li><a href="<?php $page =get_page_by_title('המוצרים המועדפים שלי'); echo get_permalink($page->ID);?>">המוצרים המועדפים שלי
			<?php if (!empty($amounts['favorites']))
				echo '('.$amounts['favorites'].')';?>
			</a></li>
			<li><a href="<?php echo wp_logout_url(get_bloginfo('url'));?>">התנתק</a></li>
		</ul>
	</div>
	<?php
}

function buki_mgr_menu() {
	?>
	<div id="user_menu">
		<ul>
			<li><a href="<?php $page = get_page_by_title('פאנל ניהול'); echo get_permalink($page->ID);?>">פאנל ניהול</a></li>
			<li><a href="<?php $page = get_page_by_title('מעקב הזמנות'); echo get_permalink($page->ID);?>">מעקב הזמנות</a></li>
			<li><a href="<?php $page = get_page_by_title('המוצרים שלנו'); echo get_permalink($page->ID);?>">המוצרים שלנו</a></li>
			<li><a href="<?php $page = get_page_by_title('לקוחות החברה'); echo get_permalink($page->ID);?>">לקוחות החברה</a></li>
			<li><a href="<?php echo wp_logout_url(get_bloginfo('url'));?>">התנתק</a></li>
		</ul>
	</div>
	<?php
}

function buki_flash_screen() {
	if (is_front_page()) { ?>
		<div id='splash' style='display: none'></div>
		<?php
	}
}
add_action( 'wp_footer', 'buki_flash_screen' );

function buki_fetured_add_error() {
	?>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <h2 class='f-18 text-center'>רשימת המוצרים המקודמים יכולה להכיל עד 5 מוצרים</h2>
	        <p class="text-center">
	        לא ניתן להציג את הפריט <span class='prod_name'></span> ברשימת המוצרים המקודמים<br/>
	        המוצר יצטרף לרשימת המתנה במוצרים המקודמים שלך</p>
	         
	        <p class='text-center'><a href=''>צפה במוצרים מקודמים</a></p>
	      </div>
	      <div class="modal-footer">
	      </div>
	    </div>
	  </div>
	</div>
<?php
}
add_action( 'wp_footer', 'buki_fetured_add_error' );