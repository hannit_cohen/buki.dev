<?php
// Register Custom Post Type
function create_buki_order_cpt() {

	$labels = array(
		'name'                  => _x( 'buki_orders', 'Post Type General Name', 'buki' ),
		'singular_name'         => _x( 'Order', 'Post Type Singular Name', 'buki' ),
		'menu_name'             => __( 'Orders', 'buki' ),
		'name_admin_bar'        => __( 'Order', 'buki' ),
		'archives'              => __( 'Item Archives', 'buki' ),
		'attributes'            => __( 'Item Attributes', 'buki' ),
		'parent_item_colon'     => __( 'Parent Item:', 'buki' ),
		'all_items'             => __( 'All Items', 'buki' ),
		'add_new_item'          => __( 'Add New Item', 'buki' ),
		'add_new'               => __( 'Add New', 'buki' ),
		'new_item'              => __( 'New Item', 'buki' ),
		'edit_item'             => __( 'Edit Item', 'buki' ),
		'update_item'           => __( 'Update Item', 'buki' ),
		'view_item'             => __( 'View Item', 'buki' ),
		'view_items'            => __( 'View Items', 'buki' ),
		'search_items'          => __( 'Search Item', 'buki' ),
		'not_found'             => __( 'Not found', 'buki' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'buki' ),
		'featured_image'        => __( 'Featured Image', 'buki' ),
		'set_featured_image'    => __( 'Set featured image', 'buki' ),
		'remove_featured_image' => __( 'Remove featured image', 'buki' ),
		'use_featured_image'    => __( 'Use as featured image', 'buki' ),
		'insert_into_item'      => __( 'Insert into item', 'buki' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'buki' ),
		'items_list'            => __( 'Items list', 'buki' ),
		'items_list_navigation' => __( 'Items list navigation', 'buki' ),
		'filter_items_list'     => __( 'Filter items list', 'buki' ),
	);
	$args = array(
		'label'                 => __( 'Order', 'buki' ),
		'description'           => __( 'Post Type Description', 'buki' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-palmtree',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'buki_order', $args );

}
add_action( 'init', 'create_buki_order_cpt', 0 );



/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
// function rd_duplicate_post_as_draft(){
// 	global $wpdb;
// 	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
// 		wp_die('No post to duplicate has been supplied!');
// 	}
 
// 	/*
// 	 * Nonce verification
// 	 */
// 	if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
// 		return;
 
// 	/*
// 	 * get the original post id
// 	 */
// 	$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
// 	/*
// 	 * and all the original post data then
// 	 */
// 	$post = get_post( $post_id );
 
// 	/*
// 	 * if you don't want current user to be the new post author,
// 	 * then change next couple of lines to this: $new_post_author = $post->post_author;
// 	 */
// 	$current_user = wp_get_current_user();
// 	$new_post_author = $current_user->ID;
 
// 	/*
// 	 * if post data exists, create the post duplicate
// 	 */
// 	if (isset( $post ) && $post != null) {
 
// 		/*
// 		 * new post data array
// 		 */
// 		$args = array(
// 			'comment_status' => $post->comment_status,
// 			'ping_status'    => $post->ping_status,
// 			'post_author'    => $new_post_author,
// 			'post_content'   => $post->post_content,
// 			'post_excerpt'   => $post->post_excerpt,
// 			'post_name'      => $post->post_name,
// 			'post_parent'    => $post->post_parent,
// 			'post_password'  => $post->post_password,
// 			'post_status'    => 'draft',
// 			'post_title'     => $post->post_title,
// 			'post_type'      => $post->post_type,
// 			'to_ping'        => $post->to_ping,
// 			'menu_order'     => $post->menu_order
// 		);
 
// 		/*
// 		 * insert the post by wp_insert_post() function
// 		 */
// 		$new_post_id = wp_insert_post( $args );
 
// 		/*
// 		 * get all current post terms ad set them to the new post draft
// 		 */
// 		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
// 		foreach ($taxonomies as $taxonomy) {
// 			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
// 			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
// 		}
 
// 		/*
// 		 * duplicate all post meta just in two SQL queries
// 		 */
// 		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
// 		if (count($post_meta_infos)!=0) {
// 			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
// 			foreach ($post_meta_infos as $meta_info) {
// 				$meta_key = $meta_info->meta_key;
// 				if( $meta_key == '_wp_old_slug' ) continue;
// 				$meta_value = addslashes($meta_info->meta_value);
// 				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
// 			}
// 			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
// 			$wpdb->query($sql_query);
// 		}
 
 
// 		/*
// 		 * finally, redirect to the edit post screen for the new draft
// 		 */
// 		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
// 		exit;
// 	} else {
// 		wp_die('Post creation failed, could not find original post: ' . $post_id);
// 	}
// }
// add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );
 
function duplicate_order() {
	global $wpdb;
	$order_id = intval($_POST['order_id']);
	$post = get_post( $order_id );
	
	if (isset( $post ) && $post != null) {
		$args = array(
			'post_author'    => 1,
			'post_content'   => $post->post_content,
			'post_name'      => $post->post_name .' Copy',
			'post_status'    => 'publish',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
	
		$new_post_id = wp_insert_post( $args );

		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$order_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}

		update_post_meta( $new_post_id, 'status', 'בהמתנה');	
		update_post_meta( $new_post_id, 'total price', '');	
		update_post_meta( $new_post_id, 'supply_date', '');	
	}
	echo json_encode(array('status' => 'success', 'msg' => 'ההזמנה שוכפלה בהצלחה. ניתן לראות אותה באזור הזמנות בהמתנה '.$new_post_id));
	exit;
}
add_action('wp_ajax_duplicate_order', 'duplicate_order');
add_action('wp_ajax_nopriv_duplicate_order', 'duplicate_order');

add_action('wp_ajax_nopriv_get_subs', 'buki_get_sub_cats');
add_action('wp_ajax_get_subs', 'buki_get_sub_cats');
function buki_get_sub_cats() {
	$cat_id = intval($_POST['cat_id']);
	$cats = get_terms(array('taxonomy' => 'product_cat', 'child_of' => $cat_id));
	$html = '<option value="">בחר תת קטגוריה</option>';
	foreach ($cats as $cat) {
		$html .= '<option value="'.$cat->term_id.'">'.$cat->name.'</option>';
	}
	echo $html;
	exit;
}

add_action('wp_ajax_get_products', 'buki_get_products');
function buki_get_products() {
	$cat_id = intval($_POST['cat_id']);
	$products = new WP_Query(array('post_type' => 'product', 
		'tax_query' => array(array('taxonomy' => 'product_cat', 'field' => 'term_id', 'terms' => $cat_id))));
	$html = '<option value="">בחר מוצר</option>';
	if ($products->have_posts()): while($products->have_posts()): $products->the_post();
		global $post;
		$html .= '<option value="'.$post->ID.'">'.$post->post_title.'</option>';
	endwhile; 
	else:
		echo 'No products';
	endif;
	echo $html;
	exit;
}

add_action('wp_ajax_get_attrs', 'buki_get_product_attribures');
function buki_get_product_attribures() {
	$prod_id = intval($_POST['prod_id']);
	$res = array();
	$res['colors'] = '<option value="">בחר צבע</option>';
	$colors = get_the_terms( $prod_id, 'product_color' );
	foreach ($colors as $color) {
		$res['colors'] .= '<option value="'.$color->name.'">'.$color->name.'</option>';
	}

	$res['code'] = get_field('ItemKey', $prod_id);
	$res['opens'] = '<option value="">בחר פתיחה</option>';
	$opens = get_the_terms( $prod_id, 'product_open' );
	foreach ($opens as $o) {
		$res['opens'] .= '<option value="'.$o->name.'">'.$o->name.'</option>';
	}

	$res['package_amount'] = get_field('package_amount', $prod_id);
	if (empty($res['package_amount']))
		$res['package_amount'] = 1;
		// get_field('package_amount', $prod_id);
	$res['price_per_piece'] = get_field('price_per_piece', $prod_id);
	$res['img'] = wp_get_attachment_image_src(get_post_thumbnail_id($prod_id), 'product_thumb')[0];
	echo json_encode($res);
	exit;
}

function buki_create_new_order() {
	$user = wp_get_current_user();
	$postarr = array(
		'post_title' => 'New order',
		'post_content' => '',
		'post_type' => 'buki_order',
		'post_status' => 'publish',
		'meta_input' => array(
			'user' => $user->ID,
			'status' => 'בהמתנה'
		)
	);
	$pid = wp_insert_post( $postarr);
	$est_total = 0;
	$total_amount = 0;
	if (!is_wp_error($pid )) {
		foreach ($_POST['products']['prod'] as $i => $prod):
			if (empty($prod))
				continue;
			$prod_id = intval($prod);
			$prod_price = get_field('price_per_piece', $prod_id);
			$color = sanitize_text_field($_POST['products']['color'][$i] );
			$open = sanitize_text_field($_POST['products']['open'][$i] );
			$amount = sanitize_text_field($_POST['products']['amount'][$i] );
			add_row('field_598f483661731', array('product' => $prod_id, 'color' => $color, 'open' => $open, 'amount' => $amount), $pid);
			$total_amount += $amount;
			$est_total += $amount * $prod_price;
		endforeach;
		update_post_meta( $pid, 'estimated_total', $est_total);
		update_post_meta( $pid, 'flours_amount', $total_amount);
		return $pid;
	} else {
		die('error occured');
		return -1;
	}
}

function update_order_data() {
	$user = wp_get_current_user();
	$meta = array();
	$order_id = intval($_POST['order_id']);
	if (!isset($_POST['self_pickup']) || $_POST['self_pickup'] != 'on') {
		$date = sanitize_text_field($_POST['supply_date'] );
		$datearr = explode('/', $date);
		$date_str = $datearr[2].$datearr[1].$datearr[0];
		$meta['supply_date'] = $date_str;
		$meta['hour'] = intval($_POST['hour']);
		$meta['minute'] = intval($_POST['minute']);
	}
	if (!isset($_POST['change_address'])) {
		$fname = sanitize_text_field($_POST['fname'] );
		$lname = sanitize_text_field($_POST['lname'] );
		$phone = sanitize_text_field($_POST['phone'] );
		$city = sanitize_text_field($_POST['city'] );
		$street = sanitize_text_field($_POST['street'] );
		$number = sanitize_text_field($_POST['number'] );
		$delivery_address = $street.' '.$number.' ,'.$city;
	} else {
		$fname = $user->user_firstname;
		$lname = $user->user_lastname;
		$phone = get_field('user_phone1', 'user_'.$user->ID);
		$city = get_field('city', 'user_'.$user->ID);
		$address = get_field('address', 'user_'.$user->ID);
		$zip = get_field('zip', 'user_'.$user->ID);
		$delivery_address = $address.' ,'.$city.' '.$zip;
	}
	$meta['fname'] = $fname;
	$meta['lname'] = $lname;
	$meta['phone'] = $phone;
	$meta['delivery_address'] = $delivery_address;

	if (isset($_POST['change_contact']) && ($_POST['change_contact'] == 'on')) {
		$meta['contact_fname'] = sanitize_text_field($_POST['contact_fname']);
		$meta['contact_lname'] = sanitize_text_field($_POST['contact_lname']);
		$meta['contact_phone'] = sanitize_text_field($_POST['contact_phone']);
	} else {
		$meta['contact_fname'] = get_field('contact_fname', 'user_'.$user->ID);
		$meta['contact_lname'] = get_field('contact_lname', 'user_'.$user->ID);
		$meta['contact_phone'] = get_field('contact_phone', 'user_'.$user->ID);
	}
	
	if (isset($_POST['comments']) && !empty($_POST['comments']))
		$meta['comments'] = sanitize_text_field($_POST['comments'] );

	foreach ($meta as $key=>$value) {
		update_post_meta( $order_id, $key, $value);
	}
	return $order_id;
}


function get_order_data($order_id) {
	$user_id = get_field('user', $order_id);
	if (is_array($user_id)) {
		$user_id = $user_id['ID'];
	} elseif (is_object($user_id)) {
		$user_id = $user_id->ID;
	}
	$user = get_userdata( $user_id );
	$data = array();
	$fname = get_field('fname', $order_id);
	if (empty($fname)) {
		$data['fname'] = $user->first_name;
		$data['lname'] = $user->last_name;
		$data['phone'] = get_field('user_phone1', 'user_'.$user_id);
		$city = get_field('city', 'user_'.$user_id);
		$address = get_field('address', 'user_'.$user_id);
		$zip = get_field('zip', 'user_'.$user_id);
		$data['delivery_address'] = implode(', ', array($address, $city)).' '.$zip;
	} else {
		$data['fname'] = get_field('fname', $order_id);
		$data['lname'] = get_field('lname', $order_id);
		$data['delivery_address'] = get_field('delivery_address', $order_id);
		$data['phone'] = get_field('phone', $order_id);
	}
	
	$contact_fname = get_field('contact_fname', $order_id);
	if (empty($contact_fname)) {
		$data['contact_fname'] = get_field('contact_fname', 'user_'.$user_id);
		$data['contact_lname'] = get_field('contact_lname', 'user_'.$user_id);
		$data['contact_phone'] = get_field('contact_phone', 'user_'.$user_id);
	} else {
		$data['contact_fname'] = get_field('contact_fname', $order_id);
		$data['contact_lname'] = get_field('contact_lname', $order_id);
		$data['contact_phone'] = get_field('contact_phone', $order_id);
	}
	$data['comments'] = get_field('comments', $order_id);
	return $data;
}

function buki_get_user_amounts() {
	$amounts = array ('waiting' => '', 'favorites' => '');
	$args = array('post_type' => 'buki_order', 'posts_per_page' => -1,
		'meta_query' => array(array('key' =>'user', 'value' => get_current_user_id()),
			array('key' => 'status', 'value' => 'בהמתנה')));
	$orders = new WP_Query($args);
	if ($orders->have_posts()) {
		$amounts['waiting'] = $orders->found_posts;
	}

	$favs = get_favorites_arr();
	if (is_array($favs)) {
		$amounts['favorites'] = count($favs);
	}

	return $amounts;
}