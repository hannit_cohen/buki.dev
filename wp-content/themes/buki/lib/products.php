<?php

// Register Custom Post Type
function create_product_cpt() {

	$labels = array(
		'name'                  => _x( 'Products', 'Post Type General Name', 'buki' ),
		'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'buki' ),
		'menu_name'             => __( 'Products', 'buki' ),
		'name_admin_bar'        => __( 'Product', 'buki' ),
		'archives'              => __( 'Item Archives', 'buki' ),
		'attributes'            => __( 'Item Attributes', 'buki' ),
		'parent_item_colon'     => __( 'Parent Item:', 'buki' ),
		'all_items'             => __( 'All Items', 'buki' ),
		'add_new_item'          => __( 'Add New Item', 'buki' ),
		'add_new'               => __( 'Add New', 'buki' ),
		'new_item'              => __( 'New Item', 'buki' ),
		'edit_item'             => __( 'Edit Item', 'buki' ),
		'update_item'           => __( 'Update Item', 'buki' ),
		'view_item'             => __( 'View Item', 'buki' ),
		'view_items'            => __( 'View Items', 'buki' ),
		'search_items'          => __( 'Search Item', 'buki' ),
		'not_found'             => __( 'Not found', 'buki' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'buki' ),
		'featured_image'        => __( 'Featured Image', 'buki' ),
		'set_featured_image'    => __( 'Set featured image', 'buki' ),
		'remove_featured_image' => __( 'Remove featured image', 'buki' ),
		'use_featured_image'    => __( 'Use as featured image', 'buki' ),
		'insert_into_item'      => __( 'Insert into item', 'buki' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'buki' ),
		'items_list'            => __( 'Items list', 'buki' ),
		'items_list_navigation' => __( 'Items list navigation', 'buki' ),
		'filter_items_list'     => __( 'Filter items list', 'buki' ),
	);
	$args = array(
		'label'                 => __( 'Post TypeProduct', 'buki' ),
		'description'           => __( 'Post Type Description', 'buki' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-palmtree',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'product', $args );

}
add_action( 'init', 'create_product_cpt', 0 );


// Register Custom Taxonomy
function tax_product_category() {

	$labels = array(
		'name'                       => _x( 'Product Categories', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Product Category', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Product Categories', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'product_cat', array( 'product' ), $args );

}
add_action( 'init', 'tax_product_category', 0 );

// Register Custom Taxonomy
function tax_product_color() {

	$labels = array(
		'name'                       => _x( 'Product Colors', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Product Color', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Product Colors', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'product_color', array( 'product' ), $args );

}
add_action( 'init', 'tax_product_color', 0 );

function tax_product_season() {

	$labels = array(
		'name'                       => _x( 'Product Seasons', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Product Season', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Product Seasons', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'product_season', array( 'product' ), $args );

}
add_action( 'init', 'tax_product_season', 0 );

function tax_product_open() {

	$labels = array(
		'name'                       => _x( 'Product Open', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Product Open', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Product Open', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'product_open', array( 'product' ), $args );

}
add_action( 'init', 'tax_product_open', 0 );

function tax_product_length() {

	$labels = array(
		'name'                       => _x( 'Product Length', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Product Length', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Product Length', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'product_length', array( 'product' ), $args );

}
add_action( 'init', 'tax_product_length', 0 );

function hp_featured_products() {
	for ($i=0; $i <3; $i++) { ?>
		<div class="product text-center">
			<?php // the_post_thumbnail('product_slider' ); ?>
			<img src='<?php echo get_stylesheet_directory_uri();?>/images/plant.jpg' />
			<div class="prod-data text-right">
				<div class="row">
					<div class="col-md-8">
						<h2>קרמיקה אובלית 20*20 ס"מ אפור</h2>
						<p class='info'>מק”ט: 213452365</p>
					</div>
					<div class="col-md-4">
						<a href='' class='yellowbg buki-btn'>לקטלוג ></a>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

function product_box($favorite = false) {
	$featured = (get_field('featured')) ? 'featured' : '';
	?>
	<div <?php post_class($featured); ?> data-id="<?php the_id(); ?>">
		<div class="prod-img">
			<?php the_post_thumbnail('product-thumb' ); ?>
			<?php if (!empty($featured)) { ?>
				<div class="featured-sticker">מומלץ</div>
			 <?php } 
			 if (!is_user_logged_in()) { ?>
			 	<div class="hover">
			 		<div class="text-center">
			 			<p class='white'>על מנת להזמין מוצר זה עליך להיות רשום</p>
			 			<a href='#' class='yellowbg buki-btn upnlogin' >התחבר ></a>
			 		</div>
			 	</div>
			 <?php }  else {  ?>
			 <div class="fav-container">
			 	<?php if ($favorite)
			 		echo '<i class="fa fa-heart" aria-hidden="true"></i>';
			 	else
			 		echo '<i class="fa fa-heart-o" aria-hidden="true"></i>';
			 	?>
			 </div>
			 <?php } ?>
		</div>
		<div class="prod-data">
			<p class='f-18 title'><?php the_title(); ?></p>
			<p class='f-16'>מק"ט: <span class='serial'><?php the_field('ItemKey');?></span></p>
		</div>
	</div>
	<?php
}

function catalog_product_box($favorite = false) {
	$featured = (get_field('featured')) ? 'featured' : '';
	?>
	<div <?php post_class($featured.' catalog_box'); ?> data-id="<?php the_id(); ?>">
		<div class="prod-img">
			<?php if (has_post_thumbnail()) {
			 	the_post_thumbnail('product_thumb' );
			} else {
				echo '<img src="'.get_template_directory_uri().'/images/grey-lotus-md.png" />';
			} ?>
			<?php if (!empty($featured)) { ?>
				<div class="featured-sticker">מומלץ</div>
			 <?php } 
			 if (!is_user_logged_in()) { ?>
			 	<div class="hover">
			 		<div class="text-center">
			 			<p class='white'>על מנת להזמין מוצר זה עליך להיות רשום</p>
			 			<a href='#' class='yellowbg buki-btn upnlogin' >התחבר ></a>
			 		</div>
			 	</div>
			 <?php }  else {  ?>
			 	<div class="hover">
					<div class="fav-container">
				 		<?php if ($favorite)
				 			echo '<i class="fa fa-heart" aria-hidden="true"></i>';
				 		else
				 			echo '<i class="fa fa-heart-o" aria-hidden="true"></i>';
				 		?>
				 	</div>			 
			 		<a href='' id='add_to_basket' class='buki-btn yellowbg white'>הוסף לסל</a>
			 	</div>
			 <?php } ?>
		</div>
		<div class="prod-data">
			<p class='f-18 title'><?php the_title(); ?></p>
			<p class='f-16'>מק"ט: <span class='serial'><?php the_field('ItemKey');?></span></p>
		</div>
	</div>
	<?php
}



function product_line() {
	?>
	<div class="product-full-line order_line clearfix" data-id="<?php the_id();?>">
		<div class="row">
			<div class="col-md-2">
			<?php
				$featured = get_field('featured');
				if ($featured)
					echo '<i class="fa fa-star" aria-hidden="true"></i>';
				else
					echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
				?>

				<?php the_post_thumbnail('product_small_thumb'); ?>
			</div>
			<div class="col-md-3">
				<h2 class='f-24 name'><?php the_title();?></h2>
				<p><?php the_field('ItemKey');?></p>
			</div>
			<div class="col-md-18">
				<?php $terms = get_the_terms(get_the_id(), 'product_cat');
				if (is_array($terms))
					echo $terms[0]->name;
				?>
			</div>
			<div class="col-md-1">
				<?php
				if (isset($terms[1]))
					echo $terms[1]->name;
				?>
			</div>
			<div class="col-md-1">
				<span class="dropdown-toggle" id="colorsMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">צבעים <i class="fa fa-angle-down" aria-hidden="true"></i></span>
				 <ul class="dropdown-menu" aria-labelledby="colorsMenuLink" id='sort_picker'>
					<?php the_terms(get_the_id(), 'product_color');	?>
				</ul>
			</div>
			<div class="col-md-18">
				<span class="dropdown-toggle" id="openMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">מצבי פתיחה <i class="fa fa-angle-down" aria-hidden="true"></i></span>
				 <ul class="dropdown-menu" aria-labelledby="openMenuLink" id='sort_picker'>
					<?php the_terms(get_the_id(), 'product_open');	?>
				</ul>
			</div>
			<div class="col-md-1">
				<span class="dropdown-toggle" id="seasonMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">עונות <i class="fa fa-angle-down" aria-hidden="true"></i></span>
				 <ul class="dropdown-menu" aria-labelledby="seasonMenuLink" id='sort_picker'>
					<?php the_terms(get_the_id(), 'product_season');	?>
				</ul>
			</div>
			<div class="col-md-1">
				<span class="dropdown-toggle" id="lengthMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">אורך <i class="fa fa-angle-down" aria-hidden="true"></i></span>
				 <ul class="dropdown-menu" aria-labelledby="lengthMenuLink" id='sort_picker'>
					<?php the_terms(get_the_id(), 'product_length');	?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-8 text-left metadata">
				<span>כמות לחבילה <?php the_field('package_amount');?></span>
				<span>מחיר ליחידה <?php the_field('price_per_piece');?> ₪</span>
			</div>
		</div>
	</div>
	<?php
}

function add_to_favorites() {
	$user_id = intval($_POST['user_id']);
	$prod_id  =  intval($_POST['product_id']);
	$res = add_row('field_59868cf503863', array('product' => $prod_id),  'user_'.$user_id);
	$user_favs = get_field('field_59868cf503863', 'user_'.$user_id);
	echo json_encode(array('status' => 'success', 'msg' => 'המוצר נוסף למועדפים בהצלחה', 'favs' => $user_favs));
	exit;
}
add_action('wp_ajax_favorites', 'add_to_favorites');

function remove_from_favorites() {
	$user_id = intval($_POST['user_id']);
	$prod_id  =  intval($_POST['product_id']);
	$user_favs = get_field('field_59868cf503863', 'user_'.$user_id);
	foreach ($user_favs as $key => $fav) {
		if (intval($fav['product']) == $prod_id){
			$found = $key;
			break;
		}
	}
	if (isset($found)) {
	    delete_row( 'field_59868cf503863', $found+1	, 'user_'.$user_id );
	}
	$user_favs = get_field('field_59868cf503863', 'user_'.$user_id);
	echo json_encode(array('status' => 'success', 'msg' => 'המוצר נוסף למועדפים בהצלחה', 'favs' => $user_favs));
	exit;
}
add_action('wp_ajax_favorites_remove', 'remove_from_favorites');

function get_favorites_arr() {
	$user_id = get_current_user_id();
	$favs = array();
	$user_favs = get_field('field_59868cf503863', 'user_'.$user_id);
	if (is_array($user_favs)) {
		foreach ($user_favs as $key => $fav) {
			$favs[] = $fav['product'];
		}
	}
	return $favs;
}

function buki_get_my_favorites() {
	$favs = array();
	$favorites = get_field('favorites', 'user_'.get_current_user_id());
	if (!empty($favorites) && is_array($favorites)) {
		foreach ($favorites as $row) {
			$favs[] = $row['product'];
		}
	}
	return $favs;
}

function get_featured_count() {
	$products = new WP_Query(array('post_type' => 'product', 'meta_key' => 'featured', 'meta_value' => 1));
	if ($products->have_posts())
		return $products->found_posts;
	else
		return 0;
	wp_reset_postdata();
	
}
function buki_set_featured() {
	$prod_id  =  intval($_POST['product_id']);
	$featured_num = get_featured_count();
	if ($featured_num >= 5) {
		echo json_encode(array('status' => 'failed'));	
		exit;
	} else {
		update_field('field_598008b66be5f', 1, $prod_id);
		echo json_encode(array('status' => 'success'));	
		exit;
	}
}
add_action('wp_ajax_featured', 'buki_set_featured');

function buki_remove_featured() {
	$prod_id  =  intval($_POST['product_id']);
	update_field('field_598008b66be5f', 0, $prod_id);
	echo json_encode(array('status' => 'success'));	
	exit;
}
add_action('wp_ajax_remove_featured', 'buki_remove_featured');

function get_catalog_args($per_page, $sort) {
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args = array('post_type' => 'product', 'paged' => $paged, 'posts_per_page' => $per_page, 
		'orderby' => array('meta_value' => 'DESC', $sort => 'ASC')/*, 'meta_key' => 'featured'*/);

	if (isset($_GET['favorites'])) {
		$args['post__in'] = $favs;
	}
	if (isset($_GET['featured'])) {
		$args['meta_query'] = array(array('key' => 'featured', 'value' => 1));
	}
	if (isset($_GET['product_cat']))
		$args ['tax_query'] = array(array('taxonomy' => 'product_cat','field' => 'term_id','terms' => intval($_GET['product_cat'])));
	if (isset($_GET['product_color']))
		$args ['tax_query'] = array(array('taxonomy' => 'product_color','field' => 'term_id','terms' => intval($_GET['product_color'])));
	if (isset($_GET['product_season'])) {
		foreach ($_GET['product_season'] as $s) {
			$seasons[] = intval($s);
		}
		$args ['tax_query'] = array(array('taxonomy' => 'product_season','field' => 'slug','terms' => $seasons));
	}

	return $args;
}
