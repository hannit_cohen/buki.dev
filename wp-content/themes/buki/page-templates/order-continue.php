<?php
/* 
 * template name: order stage 2
 */

if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url'));

$pid = buki_create_new_order();
$total = get_post_meta( $pid, 'estimated_total', 'true' );
$vat = number_format((float)$total*0.17, 2, '.', '');
$btm_line = $total + $vat;
 get_header('full');
 ?>
<article>
	<div class="container">
		<div class="row private_area_title">
			<div class="col-md-9">
				<h1 class='bb f-60'>הזמנה חדשה</h1>
			</div>
			<div class="col-md-3">
				<div class="btn-group btn-breadcrumb">
		            <a href="#" class="btn btn-default">שלב 1</a>
		            <a href="#" class="btn btn-active">שלב 2</a>
		        </div>
			</div>
		</div>
		<div class="order-info">
			<form action="<?php $page = get_page_by_title('הזמנתך נשלחה בהצלחה!'); echo get_permalink($page->ID);?>" method='post' id='order_data'>
				<div class="row">
					<div class="col-md-3">
						<div class="inner">
							<label>פרטי המזמין</label>
							<?php $user = wp_get_current_user(); ?>
							<p><?php echo $user->first_name; ?></p>
							<p><?php echo $user->last_name; ?></p>
							<p><?php the_field('user_phone1', 'user_'.$user->ID);?></p>
							<p><?php the_field('user_phone2', 'user_'.$user->ID);?></p>
							<p><?php the_field('address', 'user_'.$user->ID);?></p>
							<p><?php the_field('city', 'user_'.$user->ID);?></p>
							<p><?php the_field('zip', 'user_'.$user->ID);?></p>
							<p>
								<input type='radio' name='delivery' value='change_address' id='change_address' /> 
								<label for='change_address'>שינוי פרטי משלוח</label>
							</p>
							<p>
								<input type="radio" name='delivery' value='self_pickup' id='self_pickup' checked />
								<label for='self_pickup'>איסוף עצמי</label>
							</p>
						</div>
					</div>
					<div class="col-md-3 hidden delivery_data">
						<div class="inner">
							<label for="">שינוי פרטי משלוח</label>
							<p><input type='text' name='fname' placeholder="שם פרטי"></p>
							<p><input type='text' name='lname' placeholder="שם משפחה"></p>
							<p><input type='text' name='phone' placeholder="טלפון"></p>
							<p><input type='text' name='city' placeholder="עיר"></p>
							<div class="row row_full">
								<div class="col-md-9">
									<input type="text" name='street' placeholder="רחוב">
								</div>
								<div class="col-md-3">
									<input type="text" name='number' placeholder="מספר">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="inner">
							<label>פרטי איש קשר(בשטח):</label>
							<div class="cur_person">
								<p><?php the_field('contact_fname', 'user_'.$user->ID);?></p>
								<p><?php the_field('contact_lname', 'user_'.$user->ID);?></p>
								<p><?php the_field('contact_phone', 'user_'.$user->ID);?></p>
							</div>
							<div class="new_person hidden">
								<p><input type="text" name='contact_fname' placeholder="שם פרטי"></p>
								<p><input type="text" name='contact_lname' placeholder="שם משפחה"></p>
								<p><input type="text" name="contact_phone" placeholder="טלפון"></p>
							</div>
							<p>
								<input type='checkbox' name='change_contact' id='change_contact' />
								<label for='change_contact'>שינוי פרטי איש קשר</label>
							</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="inner">
							<label>זמני אספקה</label>
							<div class="row">
								<div class="col-md-6">
									<span>תאריך אספקה</span><br/>
									<span id='supplydate'>
										<input type='text' name="supply_date" class='datepicker'>
									</span>
								</div>
								<div class="col-md-6">
									<span>שעת אספקה</span><br/>
									<div class="row row_full delivery_time">
										<div class="col-md-6">
											<select name='minute'>
												<option value=''></option>
												<option value='00'>00</option>
												<option value='30'>30</option>
											</select>
										</div>
										<div class="col-md-6">
											<select name='hour'>
												<option></option>
												<?php foreach ( range(0,23) as $hour) {
													echo '<option value="'.$hour.'">'.$hour.'</option>';
												} ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<p>הערות למשלוח</p>
							<textarea name='comments' placeholder='כתוב הערה'></textarea>
						</div>
					</div>
				</div>
				<div class="totals">
					<label for="">סה"כ:</label> <span class="order_total"><?php echo $total;?></span> ₪<br/>
					<label for="">מע"מ:</label>
					<span class="vat"><?php echo $vat;?></span> ₪
					<div class="buttom_line"><label for="">סה”כ כולל מע”מ:</label> 
						<span class="bottom_line_total"><?php echo $btm_line;?></span> ₪
					</div>
					<p>המחירים באתר הינם צפי בלבד והמחיר הסופי הקובע הינו בתעודת המשלוח/חשבונית בלבד כפוף למחיר הפריט / גובה הפריט שסופק בפועל ביום האספקה.</p>
					<input type='submit' class='yellowbg buki-btn' name='order_submit_end' id='order_submit_end' value='שלח הזמנה >' />
				</div>
				<input type="hidden" name="order_id" value="<?php echo $pid;?>">
			</form>
		</div>
	</div>
</article>
 <?php get_footer(); ?>