<?php
/*
 * template name: order thank you
 */
// if (!is_user_logged_in())
// 	wp_redirect(get_bloginfo('url'));

$order_id = update_order_data();
get_header('full');
?>
<article>
	<div class="container">
		<div class="private_area_title">	
			<h1 class='bb f-60'>הזמנתך נשלחה בהצלחה!</h1>
		</div>
		<p class='f-24'>מספר הזמנה <?php echo $order_id;?> נשלחה בהצלחה. <br/>
		לנוחיותך, ניתן לצפות בפרטי ההזמנה ובסטטוס שלה באיזור האישי שלך. <a href='<?php echo get_permalink($order_id);?>'>צפה בהזמנה</a></p>
		<p><a href="<?php $page =get_page_by_title('קטלוג'); echo get_permalink($page->ID);?>" class="buki-btn yellowbg white">בחזרה לקטלוג ></a></p>
	</div>
</article>

 <?php get_footer(); ?>