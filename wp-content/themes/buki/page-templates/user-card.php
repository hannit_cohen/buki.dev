<?php
/* 
 * template name: User Card
 */

if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url'));

if (!isset($_GET['user_id']) || empty($_GET['user_id'])) {
	$page = get_page_by_title('רשימת לקוחות'); 
	wp_redirect(get_permalink($page->ID));
}
$uid = intval($_GET['user_id']);
$user = get_userdata($uid);
$city = get_field('city', 'user_'.$uid);
$address = get_field('address', 'user_'.$uid);
$zip = get_field('zip', 'user_'.$uid);
$delivery_address = implode(', ', array($address, $city)).' '.$zip;
get_header();
?>


<?php while ( have_posts() ) : the_post(); ?>
	<article class='private_area_title'>
		<div class="row private_area_title">
			<div class="col-md-12">
				<h1 class='bb f-60'><?php the_title(); ?></h1>
			</div>
		</div>

		<div class="order_summery">
			<div class="row">
				<div class="col-md-5">
					<p class="f-30"><?php echo $user->first_name.' '.$user->last_name;?></p>
				</div>
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-3">
							<p>איש קשר</p>
							<span><?php echo get_field('contact_fname', 'user_'.$uid).' '.get_field('contact_lname', 'user_'.$uid);?></span>
							<span><?php the_field('contact_phone', 'user_'.$uid);?></span>
						</div>
						<div class="col-md-3">
							<p>כתובת למשלוח</p>
							<span><?php echo $delivery_address;?></span>
						</div>
						<div class="col-md-3">
							<p>תאריך הקמה</p>
							<span><?php echo date( "d/m/Y", strtotime( $user->user_registered ) );?></span>
						</div>
						<div class="col-md-3">
							<p>תאריך הזמנה אחרון</p>
							<span></span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="private-area-data">
			<div class="row">
				<div class="col-md-2 orders_filter">
					<p class='bb'><strong>סנן לפי</strong></p>
					<div class='sidebar_filter'>
						<p><strong>תאריך הזמנה (4)</strong></p>
						<input type='radio' name='date' value='today'> היום<br/>
						<input type='radio' name='date' value='yesterday'> אתמול<br/>
						<input type='radio' name='date' value='this_week'> מהשבוע האחרון<br/>
						<input type='radio' name='date' value='last_week'> מהחודש האחרון<br/>
					</div>
					<div class="sidebar_filter">
						<p><strong>סטטוס הזמנה (3)</strong></p>
						<input type='radio' name='status' value='new'> חדשה<br/>
						<input type='radio' name='status' value='open'> פתוחה<br/>
						<input type='radio' name='status' value='closed'> סגורה<br/>
					</div>
					<div class="sidebar_filter">
						<p><strong>כמות מוצרים להזמנה: (4)</strong></p>
						<input type='radio' name='flowers_amount' value='1_20'> 1-20<br/>
						<input type='radio' name='flowers_amount' value='20_50'> 20-50<br/>
						<input type='radio' name='flowers_amount' value='50_100'> 50-100<br/>
						<input type='radio' name='flowers_amount' value='100_up'> 100 ומעלה<br/>
					</div>
					<div class="sidebar_filter">
						<p><strong>מחיר הזמנה: (3)</strong></p>
						<input type='radio' name='price' value='to_500'> 1-500 ₪<br/>
						<input type='radio' name='price' value='500_2000'> 500-2,000 ₪<br/>
						<input type='radio' name='price' value='2000_up'> 2,000 ₪ ומעלה<br/>
					</div>
				</div>
				<div class="col-md-10">
					<div>
						<div class="orders-table">
							<table class='table table-striped' id='orders_table'>
								<thead>
									<th>מספר הזמנה</th>
									<th>תאריך משלוח</th>
									<th>כתובת משלוח</th>
									<th>מס. פרחים</th>
									<th>סטטוס</th>
									<th>סה"כ מחיר</th>
									<th></th>
								</thead>
								<tbody>
									<?php
									$year = (isset($_GET['year'])) ? intval($_GET['year']) : intval(date('Y'));
									$args = array('post_type' => 'buki_order', 'posts_per_page' => -1, 'year' => $year, 
										'meta_query' => array(array('key' =>'user', 'value' => $uid)));
									$my_orders = new WP_Query($args);
									if ($my_orders->have_posts()): while($my_orders->have_posts()): $my_orders->the_post();
										$data = get_order_data(get_the_id());
										echo '<tr>';
										echo '<td><a href="'.get_permalink(get_the_id()).'">'.get_the_id().'</a></td>';
										echo '<td>'.get_field('supply_date').'</td>';
										echo '<td>'.$data['delivery_address'].'</td>';
										echo '<td>'.get_field('flours_amount').'</td>';
										echo '<td>'.get_field('status').'</td>';
										echo '<td>'.get_field('total price').'</td>';
										echo '<td><a href="" class="duplicate_order" data-id="'.get_the_id().'"><i class="fa fa-files-o" aria-hidden="true"></i> שכפל הזמנה</a></td>';
										echo '</tr>';
									endwhile; endif;
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
<?php endwhile; ?>
<?php get_footer(); ?>
 <?php get_footer(); ?>