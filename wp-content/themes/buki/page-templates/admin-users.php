<?php 
/*
 * template name: managment users
 */

/*
if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url'));

$user = wp_get_current_user();
if (in_array('buki_mgr', $user->roles) || in_array('administrator', $user->roles))
	wp_redirect(get_bloginfo('url') );
*/
 get_header('full');
?>
<article>
	<div class="container">
		<div class="row private_area_title">
			<h1 class='bb f-60'><?php the_title(); ?></h1>
		</div>
			
	
		<?php
		$paged = (get_query_var('paged' )) ? get_query_var('paged' ) : 1;
		$args = array('role' => 'Subscriber', 'number' => -1);
		if (isset($_GET['user_name']) && !empty($_GET['user_name'])) {
			$search_string = esc_attr( trim($_GET['user_name'] ) );
			$args['meta_query'] = array(
				'relation' => 'OR',
		        array(
		            'key'     => 'first_name',
		            'value'   => $search_string,
		            'compare' => 'LIKE'
		        ),
		        array(
		            'key'     => 'last_name',
		            'value'   => $search_string,
		            'compare' => 'LIKE'
		        )
    		);
		}

		if (isset($_GET['number']) && !empty($_GET['number'])) {
			$args['search'] = esc_attr(trim($_GET['number']));
			$args['search_columns'] = array( 'ID' );
		}

		$user_query = new WP_User_Query( $args );
		if ( ! empty( $user_query->results ) ) { ?>
			<table id ='users_table' class='table table-striped'>
				<thead>
					<tr>
						<th>שם לקוח</th>
						<th>שם איש קשר</th>
						<th>כתובת</th>
						<th>טלפון</th>
						<th>טלפון נייד</th>
						<th>דוא"ל</th>
					</tr>
				</thead>
				<tbody>
				<?php
				foreach ( $user_query->results as $user ) {
					$uid = $user->ID;
					$contact_name = get_field('contact_fname', 'user_'.$uid). ' '.get_field('contact_lname', 'user_'.$uid);
					$addr = get_field('address', 'user_'.$uid).' ,'.get_field('city', 'user_'.$uid).'  '.get_field('zip', 'user_'.$uid);
					$phone1 = get_field('user_phone1', 'user_'.$uid);
					$phone2 = get_field('user_phone2', 'user_'.$uid);
					$email = $user->user_email;
					echo '<tr data-id="'.$user->ID.'">';
					echo '<td>' . $user->display_name . '</td>';
					echo '<td><i class="fa fa-user" aria-hidden="true"></i> '.$contact_name.'</td>';
					echo '<td><i class="fa fa-map-marker" aria-hidden="true"></i> '.$addr.'</td>';
					echo '<td><i class="fa fa-phone" aria-hidden="true"></i> '.$phone1.'</td>';
					echo '<td><i class="fa fa-phone" aria-hidden="true"></i> '.$phone2.'</td>';
					echo '<td><i class="fa fa-evvelope" aria-hidden="true"></i> '.$email.'</td>';
					echo '</tr>';
				} ?>
				</tbody>
			</table>
		<?php }	?>
	</div>
	<script>
		jQuery(document).ready(function($) {
			var base_url='<?php $page = get_page_by_title('כרטיס לקוח'); echo get_permalink($page->ID);?>';
			$('td').click(function(event) {
				id = $(this).parents('tr').data('id');
				insertParam('user_id', id, base_url);
				// url = base_url+'&user_id='+id;
				// window.location.href = url;
			});
		});
	</script>
</article>
<?php get_footer('full'); ?>