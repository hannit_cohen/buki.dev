<?php
/* 
 * template name: Supplied orders
 */

if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url'));

$year = (isset($_GET['order_year'])) ? $_GET['order_year'] : date('Y');
get_header();
?>


<?php while ( have_posts() ) : the_post(); ?>
	<article class='private_area_title'>
		<div class="row">
			<div class="col-md-9">
				<h1 class='bb f-60'>אזור אישי</h1>
			</div>
			<div class="col-md-3 text-left hidden-xs">
				<a href="" class="small buki-btn yellowbg f-16">+ יצירת הזמנה חדשה</a>
			</div>
		</div>
		<div class="private-area-data">
			<div class="row">
				<div class="col-md-2">
					<div class="nav">
						<?php wp_nav_menu(array('theme_location' 	=> 'private_area',)); ?>
					</div>
				</div>
				<div class="col-md-10">
					<div class="orders">
						<div class="years-nav">
							<ul>
							<?php $years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM    $wpdb->posts WHERE  post_status = 'publish' ORDER BY post_date DESC"); 
							foreach($years as $y) {
								if ($year == $y)
									echo '<li class="active"><a href="'.add_query_arg("order_year", $y, get_permalink()).'">'.$y.'</a></li>';
								else
									echo '<li><a href="'.add_query_arg("order_year", $y, get_permalink()).'">'.$y.'</a></li>';
							}
							?>

							<?php //$archives = wp_get_archives(array('type' => 'yearly', 'post_type' => 'buki_order', 'echo' => 1));
							?>
							</ul>
						</div>
						<div class="orders-table" >
							<table id='orders_table' class='display responsive nowrap'>
								<thead>
									<th>מספר הזמנה</th>
									<th>תאריך משלוח</th>
									<th class='not-mobile'>כתובת משלוח</th>
									<th class='not-mobile'>מס. פרחים</th>
									<th class='not-mobile'>סטטוס</th>
									<th class='not-mobile'>סה"כ מחיר</th>
									<th class='not-mobile'></th>
								</thead>
								<tbody>
									<?php
									$args = array('post_type' => 'buki_order', 'posts_per_page' => -1, 'year' => intval($year), 
										'meta_query' => array(array('key' =>'user', 'value' => get_current_user_id()),
											array('key' => 'status', 'value' => 'סופקה')));
									$my_orders = new WP_Query($args);
									if ($my_orders->have_posts()): while($my_orders->have_posts()): $my_orders->the_post();
										$data = get_order_data(get_the_id());
										echo '<tr>';
										echo '<td><a href="'.get_permalink(get_the_id()).'">'.get_the_id().'</a></td>';
										echo '<td>'.get_field('supply_date').'</td>';
										echo '<td>'.$data['delivery_address'].'</td>';
										echo '<td>'.get_field('flours_amount').'</td>';
										echo '<td>'.get_field('status').'</td>';
										echo '<td>'.get_field('total price').'</td>';
										echo '<td><a href="" class="duplicate_order" data-id="'.get_the_id().'"><i class="fa fa-files-o" aria-hidden="true"></i> שכפל הזמנה</a></td>';
										echo '</tr>';
									endwhile; endif;
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
<?php endwhile; ?>
<?php get_footer(); ?>
 <?php get_footer(); ?>