<?php
/*
 * template name: create order
 */
if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url' ));
?>

<?php get_header('full'); ?>
<article>
	<div class="container">
		<div class="row private_area_title">
			<div class="col-md-9">
				<h1 class='bb f-60'>הזמנה חדשה</h1>
			</div>
			<div class="col-md-3">
				<div class="btn-group btn-breadcrumb">
		            <a href="#" class="btn btn-active">שלב 1</a>
		            <a href="#" class="btn btn-default">שלב 2</a>
		        </div>
			</div>
		</div>
		<form method='post' id='order_form_1' action="<?php $page = get_page_by_title('הזמנה שלב 2' ); echo get_permalink($page->ID ); ?>">
			<div class="orders repeatable">
				<div class="title-line hidden-xs">
					<span class='prod_img'>&nbsp;</span>
					<span>קטגוריה</span>
					<span>תת קטגוריה</span>
					<span>שם פריט</span>
					<span class='code_w'>קוד פריט</span>
				</div>
				<div class="order_line clearfix">
					<img src='<?php echo get_stylesheet_directory_uri();?>/images/default-thumb.jpg' class='prod_img'>
					<?php wp_dropdown_categories(array('taxonomy' =>'product_cat', 'show_option_all' => 'בחר...', 'class' => 'category', 'depth' => 1, 'hierarchical' => true, 'id'=>'')); ?>
					<select class='subcat'><option value=''>בחר...</option></select>
					<input type="text" name="products[prod_name][]" class="search-autocomplete" placeholder="בחר מוצר">
					<input type='hidden' name='products[prod][]' value='' class='products' />
					<!-- <select name='products[prod][]' class='products'><option value=''>בחר מוצר</option></select> -->
					<input type='text' disabled name='code' value='קוד ...' class='code code_w' />
					<div class="alignleft hidden-xs">
						<span>
							<label for="">מחיר ליח'</label>
							<span class="price_per_unit">0.0</span>₪
						</span>
						<span class="total_price">00.00</span>₪
						<a href='#' class='deleterow'><i class="fa fa-trash-o" aria-hidden="true"></i><br/>מחק</a>
					</div>
					<br/>
					<div class="alignleft hidden-xs sec">
						<select name='products[color][]' class='colors'>
							<option value=''>בחר צבע</option>
						</select>
						<select class='open' name='products[open][]'>
							<option value=''>בחר מצב פתיחה</option>
						</select>
						<span>כמות לחבילה <span class="pack_amount">00</span></span> 
						כמות 
						<span class="amount_wrap">
							<input type="text" name="products[amount][]" class='amount' value="0">
						</span>
					</div>
					<div class="row hidden-lg hidden-md hidden-sm boxes">
						<div class="col-xs-3">
							<span>
								<label>כמות לחבילה</label>
								<span class="pack_amount">00</span>
							</span> 
						</div>
						
						<div class="col-xs-6">
							<div class="amount_wrap mobile-wrap">
								<label>כמות</label>
								<input type="text" name="products[amount][]" class='amount' value="0">
							</div>
						</div>
						<div class="col-xs-12 text-left">
							<span class="total_price">00.00</span>₪
						</div>
					</div>
				</div>
			</div>
			<a id="Addline" class='yellowbg buki-btn' href='#'>+ הוסף פריט</a>
			<div class="totals">
				<label for="">סה"כ:</label> <span class="order_total"></span><br/>
				<label for="">מע"מ:</label><span class="vat"></span>
				<div class="buttom_line"><label for="">סה”כ כולל מע”מ:</label> <span class="bottom_line_total"></span></div>
				<div class="row">
					<div class="col-md-10">
						<p>המחירים באתר הינם צפי בלבד והמחיר הסופי הקובע הינו בתעודת המשלוח/חשבונית בלבד כפוף למחיר הפריט / גובה הפריט שסופק בפועל ביום האספקה.</p>
					</div>
					<div class="col-md-2">
						<p class="text-left">
							<a href='javascript:window.print()'>
								<i class="fa fa-print" aria-hidden="true"></i> הדפסה
							</a>
						</p>
					</div>
				</p>
				<input type='submit' class='yellowbg buki-btn' name='order_submit' id='order_submit' value='המשך לביצוע הזמנה >' />
			</div>
			
		</form>
	</div>
</article>

<?php get_footer(); ?>