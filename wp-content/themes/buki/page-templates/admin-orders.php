<?php 
/*
 * template name: managment orders
 */

/*
if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url'));

$user = wp_get_current_user();
if (in_array('buki_mgr', $user->roles) || in_array('administrator', $user->roles))
	wp_redirect(get_bloginfo('url') );
*/
 get_header('full');
?>
<article>
	<div class="container">
		<div class="row private_area_title">
			<div class="col-md-12">
				<h1 class='bb f-60'><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 orders_filter">
				<p class='clearfix'>
					<span class='alignright'><strong>סנן לפי:</strong></span>
					<span class='alignleft'><strong><a href='' id='reset_filters'>נקה הכל</a></strong></span>
				</p>
				<div class='sidebar_filter'>
					<p><strong>תאריך הזמנה (4)</strong></p>
					<input type='radio' name='date' value='today'> היום<br/>
					<input type='radio' name='date' value='yesterday'> אתמול<br/>
					<input type='radio' name='date' value='this_week'> מהשבוע האחרון<br/>
					<input type='radio' name='date' value='this_month'> מהחודש האחרון<br/>
				</div>
				<div class="sidebar_filter">
					<p><strong>סטטוס הזמנה (3)</strong></p>
					<input type='radio' name='status' value='new'> חדשה<br/>
					<input type='radio' name='status' value='open'> פתוחה<br/>
					<input type='radio' name='status' value='closed'> סגורה<br/>
				</div>
				<div class="sidebar_filter">
					<p><strong>כמות מוצרים להזמנה: (4)</strong></p>
					<input type='radio' name='flowers_amount' value='1_20'> 1-20<br/>
					<input type='radio' name='flowers_amount' value='20_50'> 20-50<br/>
					<input type='radio' name='flowers_amount' value='50_100'> 50-100<br/>
					<input type='radio' name='flowers_amount' value='100_up'> 100 ומעלה<br/>
				</div>
				<div class="sidebar_filter">
					<p><strong>מחיר הזמנה: (3)</strong></p>
					<input type='radio' name='price' value='to_500'> 1-500 ₪<br/>
					<input type='radio' name='price' value='500_2000'> 500-2,000 ₪<br/>
					<input type='radio' name='price' value='2000_up'> 2,000 ₪ ומעלה<br/>
				</div>
			</div>
			<div class="col-md-10">
				<?php
				$args = array('post_type' => 'buki_order', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DESC');
				$orders = new WP_Query($args);
				if ($orders->have_posts()):  ?>
					<table class='table table-striped' id='orders_table'>
						<thead>
							<tr>
								<th>מספר הזמנה</th>
								<th>תאריך הזמנה</th>
								<th>תאריך משלוח</th>
								<th>שם לקוח</th>
								<th>כתובת משלוח</th>
								<th>מוצרים</th>
								<th>סטטוס</th>
								<th>הערות</th>
								<th>מחיר</th>
							</tr>
						</thead>
						<tbody>
						<?php
						while ($orders->have_posts()): $orders->the_post();
							$data = get_order_data(get_the_id());
							echo '<tr>';
							echo '<td class="text-center"><a href="'.get_permalink(get_the_id()).'">'.get_the_id().'</a></td>';
							echo '<td>'.get_the_date('d/m/Y' ).'</td>';
							echo '<td>'.get_field('supply_date').'</td>';
							echo '<td>'.$data['fname'].' '.$data['lname'].'</td>';
							echo '<td>'.$data['delivery_address'].'</td>';
							echo '<td>'.get_field('flours_amount').'</td>';
							echo '<td>'.get_field('status').'</td>';
							$comment = get_field('comments');
							if (!empty($comment))
								echo '<td><i class="fa fa-file-text-o" aria-hidden="true"></i></td>';
							else
								echo '<td></td>';
							echo '<td>'.get_field('estimated_total').' ₪</td>';
							echo '</tr>';
						endwhile; 
						?>
						</tbody>
					</table>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>

<?php get_footer('full'); ?>