<?php
/*
 * template name: our products
 */
if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url'));

$user = wp_get_current_user();
if (!in_array('buki_mgr', $user->roles) && !in_array('administrator', $user->roles))
	wp_redirect(get_bloginfo('url') );

$sort_text = array('serial' => 'מספר קטלוגי', 'name' => 'שם');
get_header('full'); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<article>
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-2">
						<h1 class='f-60 bb'>רשימת מוצרים</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 catalog_sidebar">
						<?php get_sidebar('catalog' ); ?>
					</div>
					<div class="col-md-10">
						<?php
						$favs = buki_get_my_favorites();
						$per_page =(isset($_GET['items'])) ? $_GET['items'] : 10;
						$sort =(isset($_GET['sort'])) ? $_GET['sort'] : 'serial';
						$args = get_catalog_args($per_page, $sort);
						// var_dump($args);
						$products = new WP_Query($args);
						 ?>
						<div class="tagline clearfix">
							<div class="alignright">
								<span>מציג <?php echo $products->found_posts;?> מוצרים </span> | 
								<span class='dropdown prod_selector_wrap'>
									<span class="dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<?php echo $per_page;?> מוצרים לעמוד <i class="fa fa-caret-down" aria-hidden="true"></i>
									</span>
									 <ul class="dropdown-menu" id='items_picker' aria-labelledby="dropdownMenu1">
										<li> <a href='' data-items='10'>10 מוצרים לעמוד</a></li>
										<li> <a href='' data-items='20'>20 מוצרים לעמוד</a></li>
										<li> <a href='' data-items='40'>40 מוצרים לעמוד</a></li>
										<li> <a href='' data-items='80'>80 מוצרים לעמוד</a></li>
									</ul>
								</span>
							</div>
							<div class="alignleft">
								<span class='prod_selector_wrap dropdown'>
									<span class="dropdown-toggle" id="sortMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										מיון לפי: <?php echo $sort_text[$sort];?> <i class="fa fa-caret-down" aria-hidden="true"></i>
									</span>
									 <ul class="dropdown-menu" aria-labelledby="sortMenuLink" id='sort_picker'>
										<li> <a href='' data-sort='serial'>מספר קטלוגי</a></li>
										<li> <a href='' data-sort='name'>שם פריט</a></li>
									</ul>
								</span>
							</div>
						</div>
						<div class="products">
							<div class="row">
								<?php
								$i = 0;
								if ($products->have_posts()): while ($products->have_posts()): 
									$products->the_post(); 
									$i++;
									product_line(in_array(get_the_id(), $favs));
								endwhile; endif; ?>
							</div>
						</div>
						<?php wp_pagenavi(array('query' => $products)); 
						wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</article>
	<?php endwhile; // end of the loop. ?>

<?php get_footer('full'); ?>
