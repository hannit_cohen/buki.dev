<?php
/*
template name: about
 */

get_header('full'); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<article>
			<div class='arthead'>
				<?php the_post_thumbnail('full' ); ?>
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="part1">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<h2 class='text-center'><?php the_field('top_row_title');?></h2>
						</div>
						<div class="col-md-6 col-12-xs "><?php the_field('top_row_right');?></div>
						<div class="col-md-6 col-12-xs "><?php the_field('top_row_left');?></div>
					</div>
				</div>
			</div>
			<div class="part2">
				<div class="container-fluid">
					<div class="row boxes">
						<div class="col-md-9 col-12-xs ">
							<div class="row">
								<div class="col-md-6 col-12-xs reddish">
									<?php the_field('second_text_box')?>
								</div>
								<div class="col-md-6 col-12-xs greyish">
									<?php the_field('third_text_box')?>
								</div>
							</div>
						</div>
						<?php
							$id = get_field('side-image');
							$src = wp_get_attachment_image_src( $id, 'full')[0];
							?>
						<div class="col-md-3">
							<div class="row">
								<div class="col-md-12 about-btm-img" style='background:url(<?php echo $src;?>) no-repeat center center; background-size:cover;'></div>
							</div>
							<?php
							?>
						</div>
					</div>
				</div>
			</div>
		</article>


	<?php endwhile; // end of the loop. ?>

<?php get_footer('full'); ?>
