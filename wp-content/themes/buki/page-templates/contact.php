
<?php
/*
template name: Contact
 */

get_header('full'); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<article>
			<div id="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3381.5429102578037!2d34.84245608471741!3d32.0545615811961!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151d4a60765a91c7%3A0xa59656224c071ead!2z16bXlCLXnCAxMCwg16jXnteqINeS158!5e0!3m2!1siw!2sil!4v1501476560746" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div id="data" class='part1'>
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<h1><?php the_title(); ?></h1>
						</div>
						<div class="col-md-7 col-xs-12">
							<div class="form-wrap">
								<?php echo do_shortcode('[contact-form-7 id="32" title="contact page"]'); ?>
							</div>
						</div>
						<div class="col-md-5">
							<div class="row">
								<div class="col-md-4">
									<div class="address">
										<img src='<?php echo get_stylesheet_directory_uri();?>/images/icon-address-grey.png' />
										<p>בקרו אותנו<br/> כתובת: צה"ל 10, כפר אז"ר מיקוד: 55905</p>
									</div>
								</div>
								<div class="col-md-4">
									<div class="contact">
										<img src='<?php echo get_stylesheet_directory_uri();?>/images/icon-contact-grey.png' />
										<p>צרו קשר<br/>
										<i class="fa fa-phone" aria-hidden="true"></i> <a href='tel:035621145'>03-5621145</a><br/>
										<i class="fa fa-phone" aria-hidden="true"></i> <a href='tel:035621145'>03-5621145</a><br/>
										<i class="fa fa-envelope" aria-hidden="true"></i> <a href='mailto:buki@gmail.com'>buki@gmail.com</a></p>
									</div>
								</div>
								<div class="col-md-4">
									<p class='title'><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></p>
									<p>עיקבו אחרינו:</p>
									<p  class='social'>
										<a href=''><i class="fa fa-twitter" aria-hidden="true"></i></a>
										<a href=''><i class="fa fa-facebook" aria-hidden="true"></i></a>
										<a href=''><i class="fa fa-instagram" aria-hidden="true"></i></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>


	<?php endwhile; // end of the loop. ?>

<?php get_footer('full'); ?>


