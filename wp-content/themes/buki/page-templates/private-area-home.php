<?php
/* 
template name: private area home
*/
if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url'));

$user = wp_get_current_user();
get_header('full');
?>
<?php while ( have_posts() ) : the_post(); ?>
	<article class='private_area_title'>
		<div class="row">
			<div class="col-md-10">
				<h1 class='bb f-60'>אזור אישי</h1>
			</div>
			<div class="col-md-2">
				<a href="" class="small buki-btn yellowbg f-16">+ יצירת הזמנה חדשה</a>
			</div>
		</div>
		<div class="private-area-data">
			<div class="row">
				<div class="col-md-2">
					<div class="nav">
						<?php wp_nav_menu(array('theme_location' 	=> 'private_area',)); ?>
					</div>
				</div>
				<div class="col-md-10">
					<div class="personal-data">
						<div class="row">
							<div class="col-md-7">
								<div class="row">
									<div class="col-md-4">
										<p><strong>שם משתמש <!-- וסיסמה -->:</strong></p>
										<p><?php echo $user->user_email;?></p>
										<!-- <p class='password'>
											<i class="fa fa-eye" aria-hidden="true"></i>
											<input type='password' value='<?php the_field('user_pass', 'user_'.$user->ID) ?>' />
										</p> -->
									</div>
									<div class="col-md-4">
										<p><strong>פרטי משתמש</strong></p>
										<p><?php echo $user->first_name;?></p>
										<p><?php echo $user->last_name;?></p>
										<p><?php the_field('user_phone1', 'user_'.$user->ID);?></p>
										<p><?php the_field('user_phone2', 'user_'.$user->ID);?></p>
									</div>
									<div class="col-md-4">
										<p><strong>כתובת</strong></p>
										<p><?php the_field('address', 'user_'.$user->ID);?></p>
										<p><?php the_field('city', 'user_'.$user->ID);?></p>
										<p><?php the_field('zip', 'user_'.$user->ID);?></p>
									</div>
								</div>
							</div>
							<div class="col-md-5"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
<?php endwhile; ?>
<?php get_footer(); ?>