<?php
/*
template name: My favorites
 */
if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url'));

get_header('full'); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<article>
			<div class="container">
				<div class="row">
					<div class="col-md-2 catalog_sidebar">
						<?php get_sidebar('catalog');?>
					</div>
					<div class="col-md-10">
						<h1 class='f-60 bb'>המועדפים שלי</h1>
						<?php
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						$per_page =(isset($_GET['items'])) ? $_GET['items'] : 10;
						$favs = buki_get_my_favorites();
						$args = array('post_type' => 'product', 'paged' => $paged, 'posts_per_page' => $per_page, 'orderby' => 'meta_value', 'meta_key' => 'featured');
						$args['post__in'] = $favs;
						if (isset($_POST['product_cat']))
							$args ['tax_query'] = array(array('taxonomy' => 'product_cat','field' => 'term_id','terms' => intval($_POST['product_cat'])));
						$products = new WP_Query($args);
						 ?>
						<div class="tagline">
							<span>מציג <?php echo $products->found_posts;?> מוצרים </span> | 
							<span class='prod_selector_wrap'>
								<span class="selector_trigger"><?php echo $per_page;?> מוצרים לעמוד</span>
								<select name='prod_selector' id='prod_selector'>
									<option value='10'>10 מוצרים לעמוד</option>
									<option value='20'>20 מוצרים לעמוד</option>
									<option value='40'>40 מוצרים לעמוד</option>
									<option value='80'>80 מוצרים לעמוד</option>
								</select>
							</span>
						</div>
						<div class="products">
							<div class="row">
								<?php
								$i = 0;
								if ($products->have_posts()): while ($products->have_posts()): $products->the_post(); $i++; ?>
									<div class='col-md-15'>
										<?php product_box(true); ?>
									</div>
								<?php endwhile; endif;
								
								wp_pagenavi(array('query' => $products)); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>


	<?php endwhile; // end of the loop. ?>

<?php get_footer('full'); ?>
