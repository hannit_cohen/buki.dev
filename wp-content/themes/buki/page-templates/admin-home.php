<?php 
/*
 * template name: managment home
 */

/*
if (!is_user_logged_in())
	wp_redirect(get_bloginfo('url'));

$user = wp_get_current_user();
if (in_array('buki_mgr', $user->roles) || in_array('administrator', $user->roles))
	wp_redirect(get_bloginfo('url') );
*/
 get_header('full');
?>
<article>
	<div class="arthead">
		<img src='<?php echo get_stylesheet_directory_uri();?>/images/main-img.jpg' />
		<h1 class='white'>פאנל ניהול פרחי בוקי</h1>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="mng_inner">
					<h2 class="f-36 bb">הזמנות</h2>
					<p><?php the_field('text1');?></p>
					<p class="text-left">
						<a href='<?php $page = get_page_by_title('מעקב הזמנות'); echo get_permalink($page->ID);?>'>
							הכנס >
						</a>
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="mng_inner">
					<h2 class="f-36 bb">פריטים</h2>
					<p><?php the_field('text2');?></p>
					<p class="text-left">
						<a href='<?php $page = get_page_by_title('המוצרים שלנו'); echo get_permalink($page->ID);?>'>
							הכנס >
						</a>
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="mng_inner">
					<h2 class="f-36 bb">לקוחות</h2>
					<p><?php the_field('text3');?></p>
					<p class="text-left">
						<a href='<?php $page = get_page_by_title('לקוחות החברה'); echo get_permalink($page->ID);?>'>
							הכנס >
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer('full'); ?>