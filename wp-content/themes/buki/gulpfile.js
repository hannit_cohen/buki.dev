var gulp = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('browserSync', function() {
  	browserSync.init({
    	server: {
      	baseDir: '.'
    	},
  	})
})

gulp.task('watch', function (){
  	gulp.watch('*.css', ['browserSync']); 
  	gulp.watch('includes/js/*.js', ['browserSync']); 
})