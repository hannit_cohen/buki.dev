<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>

<header id="masthead" class="site-header navbar navbar-default navbar-fixed-top" role="banner">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="row">
		<div class="col-md-4 col-xs-8">
			<div class="row">
				<div class="site-header-inner col-sm-12">
					<div class="site-branding">
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					</div>
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
						<span class="sr-only"><?php _e('Toggle navigation','_tk') ?> </span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 hidden-xs">
			<nav class="site-navigation hidden-xs hidden-sm">
			<?php // substitute the class "container-fluid" below if you want a wider content area ?>
				<div class="row">
					<div class="site-navigation-inner col-sm-12">
						<div class="navbar navbar-default">
							<!-- The WordPress Menu goes here -->
							<?php wp_nav_menu(
								array(
									'theme_location' 	=> 'primary',
									'depth'             => 2,
									'container'         => 'nav',
									'container_id'      => '',
									'container_class'   => 'collapse navbar-collapse',
									'menu_class' 		=> 'nav navbar-nav',
									'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
									'menu_id'			=> 'main-menu',
									'walker' 			=> new wp_bootstrap_navwalker()
								)
							); ?>

						</div><!-- .navbar -->
					</div>
				</div>
			</nav><!-- .site-navigation -->
		</div>
		<div class="col-md-4 text-left col-xs-4">
			<?php if (!is_user_logged_in()) { ?>
				<a href='#' class='yellowbg buki-btn' id='login'><i class="fa fa-user hidden-lg hidden-md" aria-hidden="true"></i><span>כניסה לאזור האישי</span></a>
			<?php } else { 
				$user = wp_get_current_user(); 
				$name = (empty($user->first_name)) ? $user->user_login : $user->first_name; ?>
				<a href='#' class='yellowbg buki-btn' id='private_area'>
					שלום <?php echo $name;?> <i class="fa fa-chevron-down" aria-hidden="true"></i>
					<span class='extra' style='display: block; font-size: 16px'>כניסה לאזור האישי</span>
				</a>
			<?php } ?>
		</div>
	</div>
	<nav class="site-navigation hidden-md hidden-lg">
		<div class="navbar navbar-default">
				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location' 	=> 'primary',
						'depth'             => 2,
						'container'         => 'nav',
						'container_id'      => 'navbar-collapse',
						'container_class'   => 'collapse navbar-collapse',
						'menu_class' 		=> 'nav navbar-nav',
						'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
						'menu_id'			=> 'main-menu',
						'walker' 			=> new wp_bootstrap_navwalker()
					)
				); ?>

		</div><!-- .navbar -->
	</div>
</nav><!-- .site-navigation -->
</header><!-- #masthead -->
