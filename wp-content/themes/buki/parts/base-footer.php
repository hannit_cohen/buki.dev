<footer id="colophon" class="site-footer" role="contentinfo">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<a href="<?php bloginfo('url');?>" id="footer-logo"></a>
			</div>
			<div class="col-md-9 col-xs-12">
				<div class="row">
					<div class="col-md-3 col-xs-4">
						<div class="address">
							<img src='<?php echo get_stylesheet_directory_uri();?>/images/map-w-marker.png' />
							<p>בקרו אותנו כתובת: צה"ל 10, כפר אז"ר מיקוד: 55905</p>
						</div>
					</div>
					<div class="col-md-3 col-xs-4">
						<div class="contact">
							<img src='<?php echo get_stylesheet_directory_uri();?>/images/icon-contact.png' />
							<p>צרו קשר<br/>
							<i class="fa fa-phone" aria-hidden="true"></i> <a href='tel:035621145'>03-5621145</a><br/>
							<i class="fa fa-phone" aria-hidden="true"></i> <a href='tel:035621145'>03-5621145</a><br/>
							<i class="fa fa-envelope" aria-hidden="true"></i> <a href='mailto:buki@gmail.com'>buki@gmail.com</a></p>
						</div>
					</div>
					<div class="col-md-3 col-xs-4">
						<div class="sitemap">
							<img src='<?php echo get_stylesheet_directory_uri();?>/images/icon-site-map.png' />
							<?php wp_nav_menu(array('theme_location' => 'sitemap') ); ?>
						</div>
					</div>
					<div class="col-md-3 col-xs-4">
						<p class='title'><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></p>
						<p>עיקבו אחרינו:</p>
						<p  class='social'>
							<a href=''><i class="fa fa-twitter" aria-hidden="true"></i></a>
							<a href=''><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<a href=''><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->
<div id="credits">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="text-right">
					© 2017 עיצוב ופיתוח להאמית קרהשק סכעיט מנכם
				</div>
			</div>
			<div class="col-md-6">
				<div class="text-left">
					<a href="" id='shaylogo'></a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php wp_footer(); ?>

</body>
</html>