<?php if (is_page_template('page-templates/admin-users.php' ) || is_page_template('page-templates/user-card.php' )) { ?>
	<div class="whitebg">
		<div id="main-filter" class="filter-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<form action="" method='get'>
						<div class="row boxes">
							<div class="col-md-5">
								<input type='text' name='user_name' placeholder="שם לקוח" />
							</div>
							<div class="col-md-5">
								<input type='text' name='number' placeholder="מספר לקוח"/>
							</div>
							<div class="col-md-2">
								<input type='submit' class='buki-btn yellowbg white search' value="חפש" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<?php } elseif(is_singular('buki_order')) { ?>
	<div class="whitebg hidden-xs">
		<div id="filter-nav" class="container">
			<div class="row">
				<div class="col-md-2">
					<a href="javascript:window.history.back()" class="alignrght text-right">
						< חזרה
					</a>
				</div>
				<div class="col-md-8">
					<div id="main-filter" class="filter-container">
						<form action="" method='post'>
							<?php 
							$selected = (isset($_POST['product_cat'])) ? $_POST['product_cat'] : '';
							wp_dropdown_categories(array('taxonomy' =>'product_cat', 'show_option_all' => 'בחר קטגוריה', 'class' => 'category', 'depth' => 1, 'hierarchical' => true)); ?>
							<select class='subcat'><option value=''>בחר תת קטגוריה</option></select>
							<input type="text" name="" placeholder="חיפוש חופשי">
							<button type="submit" class="buki-btn yellowbg white">
							    חיפוש
							    <i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</form>
					</div>
				</div>
				<div class="col-md-2">
					<a href='javascript:window.print()' class='alignleft text-left'>
						<i class="fa fa-print" aria-hidden="true"></i> הדפסה
					</a>
					
				</div>
			</div>
		</div>
	</div>
<?php } elseif(is_page_template('page-templates/admin-orders.php')) { ?>
	<div class="whitebg hidden-xs">
		<div id="filter-nav" class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="clearfix">
						<a href='javascript:window.print()' class='alignleft text-left'>
							<i class="fa fa-print" aria-hidden="true"></i> הדפסה
						</a>
						<a href="javascript:window.history.back()" class="alignrght text-right">
							< חזרה
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
<?php } else { ?>
	<div class="whitebg hidden-xs">
		<div id="main-filter" class="filter-container">
			<form action="" method='post'>
				<?php 
				$selected = (isset($_POST['product_cat'])) ? $_POST['product_cat'] : '';
				wp_dropdown_categories(array('taxonomy' =>'product_cat', 'show_option_all' => 'בחר קטגוריה', 'class' => 'category', 'depth' => 1, 'hierarchical' => true)); ?>
				<select class='subcat'><option value=''>בחר תת קטגוריה</option></select>
				<input type="text" name="" placeholder="חיפוש חופשי">
				<button type="submit" class="buki-btn yellowbg white">
				    חיפוש
				    <i class="fa fa-search" aria-hidden="true"></i>
				</button>
			</form>
		</div>
	</div>
<?php } ?>