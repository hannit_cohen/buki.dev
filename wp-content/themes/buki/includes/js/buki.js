function pad(n, width, z) {
 	z = z || '0';
  	n = n + '';
  	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function change_order_line_cost(order_line, newVal) {
	var price = parseFloat(order_line.find('.price_per_unit').text());
	var order_total = 0;
	var vat = 0;

	total = price*newVal;
	order_line.find('.total_price').text(total);

	calc_order_total();
	
}

function calc_order_total() {
	var order_total = 0;
	jQuery('.order_line .total_price').each(function() {
    	order_total += parseFloat(jQuery(this).text());
	});
	order_total = order_total / 2;
	jQuery('.order_total').text(order_total);

	// calc vat
	vat = (order_total*17/100).toFixed(2);
	jQuery('.vat').text(vat);
	btm = parseFloat(order_total) + parseFloat(vat);
	jQuery('.bottom_line_total').text(btm);
}
function insertParam(key, value, url='') {
	if (url == '') {
		url = document.location;
	} else {
		parser = document.createElement('a');
		parser.href = url;
		url = parser;
	}

    key = escape(key); value = escape(value);

    var kvp = url.search.substr(1).split('&');
    if (kvp == '') {
        document.location = url + '?' + key + '=' + value;
    }
    else {

        var i = kvp.length; var x; while (i--) {
            x = kvp[i].split('=');

            if (x[0] == key) {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }

        if (i < 0) { kvp[kvp.length] = [key, value].join('='); }

        //this will reload the page, it's likely better to store this until finished
        // alert(kvp.join('&'));
        document.location.search = kvp.join('&');
    }
}

jQuery(document).ready(function($) {
	var cur_cat;
	$('.datepicker').datepicker({
		dateFormat:'dd/mm/yy',
		 minDate : 0
	});

	// Login form add placeholders & remove labels
	$('#loginform input[type="text"]').attr('placeholder', 'שם משתמש');
	$('#loginform input[type="password"]').attr('placeholder', 'סיסמא');
	$('#loginform label[for="user_login"]').contents().filter(function() {
		return this.nodeType === 3;
	}).remove();
	$('#loginform label[for="user_pass"]').contents().filter(function() {
		return this.nodeType === 3;
	}).remove();
	
	// login form modal
	$('#login').click(function(event) {
		event.preventDefault();
		pos = $(this).offset();
		if ($('#masthead').hasClass('scroll')) {
			$('#login-box').css({'left': pos.left, 'top' : 100});	
		} else {
			$('#login-box').css({'left': pos.left, 'top' : 150});	
		}
		$('#login-box').slideToggle('slow');
	});

	// private area modal menu
	$('#private_area').click(function(event) {
		event.preventDefault();
		pos = $(this).offset();
		if ($('#masthead').hasClass('scroll')) {
			$('#user_menu').css({'left': pos.left, 'top' : 100});	
		} else {
			$('#user_menu').css({'left': pos.left, 'top' : 150});	
		}
		$('#user_menu').slideToggle('slow');
	});


	$('.selector_trigger').click(function(event) {
		$('#prod_selector').slideToggle('fast');
	});

	// show/hide user password
	$('p.password .fa-eye').click(function(event) {
		if ($(this).parent().find('input').attr('type') == 'password' )
			$(this).parent().find('input').attr('type', 'text');
		else
			$(this).parent().find('input').attr('type', 'password');
	});

	// order duplication (private area)
	$('.duplicate_order').click(function(event) {
		jQuery.ajax({
			type:"POST",
			url: MyAjax.ajaxurl,
			dataType: 'json',
			data : {
				'action' : 'duplicate_order',
				'order_id': $(this).data('id')
			},
			success:function(results){
				alert(results.msg);
			}
		});
		return false;
	});

	$('#duplicate_single_order').click(function(event) {
		jQuery.ajax({
			type:"POST",
			url: MyAjax.ajaxurl,
			dataType: 'json',
			data : {
				'action' : 'duplicate_order',
				'order_id': $(this).data('id')
			},
			success:function(results){
				alert(results.msg);
			}
		});
		return false;
	});

	// favorite product
    $('.product .fav-container .fa-heart-o').click(function(event) {
		var display = $(this);
		jQuery.ajax({
			type:"POST",
			url: MyAjax.ajaxurl,
			dataType: 'json',
			data : {
				'action' : 'favorites',
				'product_id': $(this).parents('.product').data('id'),
				'user_id': MyAjax.user_id
			},
			success:function(results){
				display.toggleClass('fa-heart fa-heart-o');
			}
		});
		return false;
	});

    $('.product .fav-container .fa-heart').click(function(event) {
		var display = $(this);
		jQuery.ajax({
			type:"POST",
			url: MyAjax.ajaxurl,
			dataType: 'json',
			data : {
				'action' : 'favorites_remove',
				'product_id': $(this).parents('.product').data('id'),
				'user_id': MyAjax.user_id
			},
			success:function(results){
				display.toggleClass('fa-heart fa-heart-o');
			}
		});
		return false;
	});

	// favorite product
    $('.product-full-line .fa-star-o').click(function(event) {
		var display = $(this);
		jQuery.ajax({
			type:"POST",
			url: MyAjax.ajaxurl,
			dataType: 'json',
			data : {
				'action' : 'featured',
				'product_id': $(this).parents('.product-full-line').data('id'),
			},
			success:function(results){
				if (results.status == 'success')
					display.toggleClass('fa-star fa-star-o');
				else {
					name = display.parents('.product-full-line').find('.name').text();
					$('#myModal .prod_name').text(name);
					$('#myModal').modal();
				}
			}
		});
		return false;
	});

    $('.product-full-line .fa-star').click(function(event) {
		var display = $(this);
		jQuery.ajax({
			type:"POST",
			url: MyAjax.ajaxurl,
			dataType: 'json',
			data : {
				'action' : 'remove_featured',
				'product_id': $(this).parents('.product-full-line').data('id'),
			},
			success:function(results){
				display.toggleClass('fa-star fa-star-o');
			}
		});
		return false;
	});

	// order line amount +/- buttons
	$("form .amount_wrap").append('<div class="inc button"><i class="fa fa-caret-up" aria-hidden="true"></i></div><div class="dec button"><i class="fa fa-caret-down" aria-hidden="true"></i></div>');

	// order line amount calc sums
	$('form').on('click', '.button', function(event) {
		var oldValue = $(this).parent().find("input").val();
		
		if ($(this).hasClass('inc')) {
		  var newVal = parseFloat(oldValue) + 1;
		} else {
			if (oldValue > 0) {
				var newVal = parseFloat(oldValue) - 1;
			} else {
				newVal = 0;
			}
		}

		$(this).parent().find("input").val(newVal);
		change_order_line_cost($(this).parents('.order_line'), newVal);
	});

	$('form').on('change paste keyup', '.amount', function(event) {
		change_order_line_cost($(this).parents('.order_line'), $(this).val());
	});

	// repetable
	var order_row = $('.repeatable .order_line').clone();
	$('#Addline').click(function(event) {
		order_row.clone().appendTo('.repeatable');
		return false;
	});

	// initial order lines
	for (var i = 0; i < 4; i++) {
		$('#Addline').click()
	}
	
	// submit order form
	$('#order_submit').click(function(event) {
		if ($('.bottom_line_total').text() == '') {
			alert('יש להוסיף מוצרים להזמנה');
			return false;
		} else
			$('#order_form_1').submit();
	});

	// delete order line
	$('form').on('click', '.deleterow', function(event) {
		$(this).parents('.order_line').remove();
		event.preventDefault();
		/* Act on the event */
	});

    // order form category change
	$('form').on('change', '.category', function(event) {
		var row;
		cur_cat = $(this).val();
		if ($('body').hasClass('home')) {
			row = $(this).parents('.filter-container');
		} else {
			row = $(this).parents('.order_line');
		}
		jQuery.ajax({
			type:"POST",
			url: MyAjax.ajaxurl,
			data : {
				'action' : 'get_subs',
				'cat_id': $(this).val(),
			},
			success:function(results){
				row.find('.subcat').html(results);
				// alert(results.msg);
			}
		});
	});

	// order form sub-category change
	$('form').on('change', '.subcat', function(event) {
		cur_cat = $(this).val();
		var row = $(this).parents('.order_line');
		jQuery.ajax({
			type:"POST",
			url: MyAjax.ajaxurl,
			data : {
				'action' : 'get_products',
				'cat_id': $(this).val(),
			},
			success:function(results){
				row.find('.products').html(results);
			}
		});
	});

	var searchRequest;
	$('form').on('click', '.search-autocomplete', function(event) {
		event.preventDefault();
		var cur_prod = $(this);
		$('.search-autocomplete').autoComplete({
			minChars: 2,
			source: function(term, suggest){
				try { searchRequest.abort(); } catch(e){}
				searchRequest = $.post(MyAjax.ajaxurl, { search: term, action: 'search_site', cat_id: cur_cat }, function(res) {
					suggest(res.data);
				});
			},
			renderItem: function (item, search){
	        	return '<div class="autocomplete-suggestion" data-id="'+item[1]+'"">'+item[0]+'</div>';
		    },
		    onSelect: function(e, term, item){
		        cur_prod.parents('.order_line').find('.products').val(item.data('id'));
		        var row = cur_prod.parents('.order_line');
		        jQuery.ajax({
		        	type:"POST",
		        	url: MyAjax.ajaxurl,
		        	dataType: 'json',
		        	data : {
		        		'action' : 'get_attrs',
		        		'prod_id': item.data('id'),
		        	},
		        	success:function(results){
		        		row.find('.code').val('קוד: '+results.code);
		        		row.find('.open').html(results.opens);
		        		row.find('.colors').html(results.colors);
		        		row.find('.pack_amount').text(results.package_amount);
		        		row.find('.price_per_unit').text(results.price_per_piece);
		        		row.find('img').attr('src', results.img);
		        	}
		        });
		    }
		});
	});
	

	// order form - product select
	// $('form').on('change', '.products', function(event) {
	// 	var row = $(this).parents('.order_line');
	// 	jQuery.ajax({
	// 		type:"POST",
	// 		url: MyAjax.ajaxurl,
	// 		dataType: 'json',
	// 		data : {
	// 			'action' : 'get_attrs',
	// 			'prod_id': $(this).val(),
	// 		},
	// 		success:function(results){
	// 			row.find('.code').val('קוד: '+results.code);
	// 			row.find('.open').html(results.opens);
	// 			row.find('.colors').html(results.colors);
	// 			row.find('.pack_amount').text(results.package_amount);
	// 			row.find('.price_per_unit').text(results.price_per_piece);
	// 			row.find('img').attr('src', results.img);
	// 		}
	// 	});
	// });

	$('#change_address,#self_pickup').change(function(event) {
		$('.delivery_data').toggleClass('hidden');
	});
	$('#change_contact').change(function(event) {
		$('.new_person').toggleClass('hidden');
		$('.cur_person').toggleClass('hidden');
	});

	$('#users_table').DataTable({
		search: false,
		pagingType:'simple_numbers',
		"dom": '<"top"il>rt<"bottom"p><"clear">',
		language: {
			search:         "חיפוש &nbsp;:",
        	lengthMenu:    "_MENU_ משתמשים",
        	sInfo: 'מציג _START_ - _END_ מתוך _TOTAL_ לקוחות'
		},
		"columns": [
			null,
		    { "width": "20%" },
		    null,
		    null,
		    null,
		    null
		  ],
		responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   -1
        } ]
	});

	var orders_table = $('#orders_table').DataTable({
		search: false,
		pagingType:'simple_numbers',
		"dom": '<"top"il>rt<"bottom"p><"clear">',
		language: {
			search:         "חיפוש &nbsp;:",
        	lengthMenu:    "_MENU_ משתמשים",
        	sInfo: 'מציג _START_ - _END_ מתוך _TOTAL_ הזמנות'
		},
		responsive: {
	        details: {
	            type: 'column',
	            target: 1
	        }
	    },
	    // columnDefs: [ {
	    //     className: 'control',
	    //     orderable: false,
	    //     targets:   1
	    // } ]
	});

	var normalizeDate = function(dateString) {
		orig = dateString.split("/");
		normalized = orig[2]+orig[1]+orig[0];
	  	return normalized;
	}

	function table_date_filter(data) {
		if ($('input[type=radio][name=date]:checked').length == 0)
			return true;

		date = $('input[type=radio][name=date]:checked').val();
		today = new Date();
		row_date = normalizeDate(data[1]);
		// row_date = new Date(data[1]);
		if (date == 'today') {
			my_date = pad(today.getDate(),2)+'/'+pad((today.getMonth()+1),2)+'/'+today.getFullYear();
			my_date = normalizeDate(my_date);
			return (my_date == row_date);
			// return (today == row_date);
		} else if (date == 'yesterday') {
			yesterday = new Date(today);
			yesterday.setDate(today.getDate() - 1);
			my_date = pad(yesterday.getDate(),2)+'/'+pad((yesterday.getMonth()+1),2)+'/'+yesterday.getFullYear();
			my_date = normalizeDate(my_date);
			return (my_date == row_date);
		} else if (date == 'this_week') {
			end = pad(today.getDate(),2)+'/'+pad((today.getMonth()+1),2)+'/'+today.getFullYear();
			end_str = normalizeDate(end);
			start = new Date(today);
			start.setDate(start.getDate() - 7);
			start_str = pad(start.getDate(),2)+'/'+pad((start.getMonth()+1),2)+'/'+start.getFullYear();
			start_str = normalizeDate(start_str);
			return ((start_str <= row_date) && (end_str >= row_date));
		} else if (date == 'this_month') {
			end = pad(today.getDate(),2)+'/'+pad((today.getMonth()+1),2)+'/'+today.getFullYear();
			end_str = normalizeDate(end);
			start = new Date(today);
			start.setMonth(start.getMonth() - 1);
			start_str = pad(start.getDate(),2)+'/'+pad((start.getMonth()+1),2)+'/'+start.getFullYear();
			start_str = normalizeDate(start_str);
			return ((start_str <= row_date) && (end_str >= row_date));
		}
		return true;

	}

	function table_status_filter(data) {
		if ($('input[type=radio][name=status]:checked').length == 0)
			return true;

		status = $('input[type=radio][name=status]:checked').val();
		if (status == 'new' && data[6] == 'בהמתנה')
			return true;
		else if (status == 'open' && data[6] == 'פתוחה')
			return true;
		else if (status == 'closed' && data[6] == 'סופקה')
			return true;
		else
			return false;
	}

	function table_flowers_amount_filter(data) {
		if ($('input[type=radio][name=flowers_amount]:checked').length == 0)
			return true;

		flowers_amount = $('input[type=radio][name=flowers_amount]:checked').val();
		row_amount = parseInt(data[5]);

		if (flowers_amount == '1_20' && row_amount >= 1 && row_amount <= 20)
			return true
		else if (flowers_amount == '20_50' && row_amount >= 20 && row_amount <= 50)
			return true;
		else if (flowers_amount == '50_100' && row_amount >= 50 && row_amount <= 100)
			return true;
		else if (flowers_amount == '100_up' && row_amount >= 100)
			return true;
		else
			return false;
	}

	function table_price_filter(data) {
		if ($('input[type=radio][name=price]:checked').length == 0)
			return true;

		price = $('input[type=radio][name=price]:checked').val();
		row_price = parseInt(data[8]);

		if (price == 'to_500' &&  row_price <= 500)
			return true
		else if (price == '500_2000' && row_price >= 500 && row_price <= 2000)
			return true;
		else if (price == '2000_up' && row_price >= 2000)
			return true;
		else
			return false;
	}
	$.fn.dataTable.ext.search.push(
	    function( settings, data, dataIndex ) {
	    	date_filter = table_date_filter(data);
	    	status_filter = table_status_filter(data);
	    	flowers_amount_filter = table_flowers_amount_filter(data);
	    	price_filter = table_price_filter(data);
	    	
	    	return date_filter && status_filter && flowers_amount_filter && price_filter;
	    }
	);


	$('#orders_table #reset_filters').click(function(event) {
		event.preventDefault();
		$('input.search_events').val('');
		$('#orders_table').dataTable().fnDraw();
		$('input[type="radio"]').prop('checked', false);
	}); 

	$('input[type=radio]').change(function() {
		orders_table.draw();
	});

	$('#products_table').DataTable({
		pagingType:'simple_numbers',
		"dom": '<"top">rt<"bottom"p><"clear">',
	});

	if ($('body').hasClass('home') ) {
		$('#splash').show();
        setTimeout(function() { $('#splash').fadeOut('fast');}, 1500);
    }

    $('#items_picker a').click(function(event) {
    	event.preventDefault();
    	val = $(this).data('items');
    	insertParam('items', val);
    });

    $('#sort_picker a').click(function(event) {
    	event.preventDefault();
    	val = $(this).data('sort');
    	insertParam('sort', val);
    });

    $('#cleanall').click(function(event) {
    	event.preventDefault();
    	$('#filter_form')[0].reset();
    });

    $('.sidebar_filter .clean_filter').click(function(event) {
    	event.preventDefault();
    	$(this).parents('.sidebar_filter').find('input[type=checkbox]:checked').removeAttr('checked');
    });

    $(window).scroll(function(event){
	  if ($(window).scrollTop() > 32) {
	  	$('body').addClass('scroll');
	  } else {
	  	$('body').removeClass('scroll');
	  }
	});

	if ($(window).scrollTop() > 32) {
	  	$('body').addClass('scroll');
	}
});