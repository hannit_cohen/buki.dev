<?php
define('WP_USE_THEMES', false);
require('./wp-load.php');

set_time_limit(0);
function product_exists($ItemKey)
{
    $q = array('post_type' => 'product', 'post_status'   => 'publish',
        'meta_query' => array(
            array(
                'key'     => 'ItemKey',
                'value'   => $ItemKey,
                'compare' => '=',
            ),
        ),
    );
    $prod = new WP_Query($q);
    if ($prod->have_posts()) {
        $prod->the_post();
        return get_the_ID();
    }
    return false;
}

?>
<html>
<head>
  <meta charset="UTF-8">
</head>
<body>
    <?php
    $cats = array (1=>'פרחים', 2=>'ורדים', 3=>'ירוקים', 4=>'עציצים', 5=>'אביזרים');
    foreach ($cats as $cat) {
        $catId = get_term_by('name', $cat, 'product_cat');
        $MyCats[$cat] = $catId->term_id;
    }

    $row = 0;
    if (($handle = fopen("buki.csv", "r")) !== false) {
        while (($data = fgetcsv($handle, 1000, ",")) !== false) {
            $row++;
            if ($row == 1) {
                continue;
            }
            $itemId = $data[1];
            $prodId = product_exists($itemId);
            $Name = iconv('Windows-1255', 'utf-8', $data[2]);

            // handle unfound products
            if (!$prodId) {
                echo "Product with key #$itemId and name $Name Not found (row $row)<br/>";
                continue;
            }

            // skip handled products
            if (has_post_thumbnail($prodId)) {
                continue;
            }

            // handle categories
            if (!empty($data[4])) {
                $catName = iconv('Windows-1255', 'utf-8', $data[4]);
                if (isset($MyCats[trim($catName)])) {
                    $catId = $MyCats[trim($catName)];
                } else {
                    $tid = get_term_by('name', $catName, 'product_cat');
                    if ($tid === false) {
                        $parent = $cats[$data[0]];
                        $tid = wp_insert_term($catName, 'product_cat', array('parent' => $MyCats[$parent]));
                        if (is_wp_error($tid)) {
                            var_dump($tid);
                            var_dump($MyCats[$parent]);
                            var_dump($catName);
                            var_dump($itemId);
                            die();
                        }
                        $catId = $tid->term_id;
                        $MyCats[trim($catName)] = $catId;
                        echo "Created new cat: $catName ($catId)<br/>";
                    } else {
                        $catId = $tid->term_id;
                        $MyCats[trim($cat)] = $catId;
                    }
                }
                wp_set_post_terms($prodId, $catId, 'product_cat', true);
            }

            $files = glob("wp-content/uploads/3/$itemId*.JPG");
            // skip unfound images
            if (empty($files)) {
                echo "Could not find thumbnail for product $itemId<br/>";
                continue;
            }

            // handle images
            $upload_dir = wp_upload_dir();
            $image_url = $files[0];
            $base_url = get_bloginfo('url');
            $image_data = file_get_contents($image_url);
            $filename = basename($image_url);
            if (wp_mkdir_p($upload_dir['path'])) {
                $file = $upload_dir['path'] . '/' . $filename;
            } else {
                $file = $upload_dir['basedir'] . '/' . $filename;
            }
            file_put_contents($file, $image_data);

            $wp_filetype = wp_check_filetype($filename, null);
            $attachment = array(
                'guid'           => $upload_dir['url'] . '/' . basename($filename),
                'post_mime_type' => $wp_filetype['type'],
                'post_title'     => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );
            $attach_id = wp_insert_attachment($attachment, $file, $prodId);
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            $attach_data = wp_generate_attachment_metadata($attach_id, $file);
            $res1= wp_update_attachment_metadata($attach_id, $attach_data);
            $res2 = set_post_thumbnail($prodId, $attach_id);
            // echo "Finised handling product $prodId ($itemId) cat: $catName<br/>";
        }
        fclose($handle);
    }
?>
</body>
</html>