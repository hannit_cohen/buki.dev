<?php
define('WP_USE_THEMES', false);
require('./wp-load.php');

$dsn = 'sqlsrv:Server=82.166.92.197;Database=lando07';
$user = 'sa';
$password = 'wizsoft';


function order_exists($ItemKey) {
  $q = array('post_type' => 'buki_order', 'post_status'   => 'publish',
    'meta_query' => array(
      array(
        'key'     => 'DocNumber',
        'value'   => $ItemKey,
        'compare' => '=',
      ),
    ),
  );
  $prod = new WP_Query($q);
  if ($prod->have_posts()){
    $prod->the_post();
    return get_the_ID();
  }
  return false;
}

function get_prod_id($ItemKey) {
  $q = array('post_type' => 'product', 'post_status' => 'publish',
    'meta_query' => array(
      array(
        'key'     => 'ItemKey',
        'value'   => $ItemKey,
        'compare' => '=',
      ),
    ),
  );
  $prod = new WP_Query($q);
  if ($prod->have_posts()){
    $prod->the_post();
    return get_the_ID();
  }
  var_dump($ItemKey);
  echo 'Not Found !!';
  var_dump($q);
  return false;
}

function get_order_user($AccountKey) {
  $q = array('meta_key' => 'AccountKey', 'meta_value' => $AccountKey, 'meta_compare' => '=');
  $users = new WP_User_Query($q);
  if ( ! empty( $users->get_results() ) ) {
    if ($users->total_users != 1) {
      echo "Error more than one user ".$users->total_users;
      die();
    }
    $user = $users->get_results()[0];
    return $user->ID;
  }
}

try {
    $dbh = new PDO($dsn, $user, $password);
    set_time_limit(0);
  // $stmt = $dbh->query("SELECT * FROM Varieties");
  // $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
  // var_dump($res)    ;
  
  // echo "************\n"  ;
    $amount = intval($_GET['amount']);
    $offset = intval($_GET['offset']);
    $next = $amount+$offset;

    $q = "WITH Seq AS ( SELECT ID, AccountKey, AccountName, DocNumber, ValueDate, Address, City,Phone, TFtal, IssueDate, Quantity, contactMail, DueDate, ROW_NUMBER() OVER(ORDER BY ID ASC) AS 'RowNumber' FROM Stock WHERE DocumentId = 6 ) SELECT ID, AccountKey, AccountName, DocNumber, ValueDate, Address, City,Phone, TFtal, IssueDate, Quantity, contactMail, DueDate FROM Seq WHERE RowNumber BETWEEN $offset AND $next";
    // $q = "SELECT AccountKey,ValueDate,Address,City,Phone,TFtal,IssueDate,Quantity,contactMail from Stock where DocumentId = 6";
    $stmt = $dbh->query($q);
   	$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
   	$i = 0;
   	foreach ($res as $o) {
      $total_qtty = 0;
      // var_dump($o);
      // echo "###<br/>";
      $uid = get_order_user($o['AccountKey']);
      $prod_q = "SELECT ItemKey,Quantity from StockMoves where StockID = ".$o['ID'];
      $products = $dbh->query($prod_q);
      $prod_res = $products->fetchAll(PDO::FETCH_ASSOC);
      $products = array();
      foreach ($prod_res as $prod) {
        if ($prod['ItemKey'] == '*')
          continue;
        $prod_id = get_prod_id($prod['ItemKey']);
        $qtt = $prod['Quantity'];
        $products[] = array('product' => $prod_id, 'amount' => $qtt);
        $total_qtty += $qtt;
      }
      // var_dump($products);
     
   	  $post_data = array(
        'post_title'    => wp_strip_all_tags( $o['DocNumber'] ),
        'post_content'  =>  $o['DocNumber'],
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type' => 'buki_order',
        'meta_input'   => array(
          'DocNumber' => $o['DocNumber'],
          'supply_date' => $o['DueDate'],
          'delivery_address' => $o['Address'].','.$o['City'],
          'flours_amount' => $total_qtty,
          'total price'  => $o['TFtal'],
          'user' => $uid,
          'fanme' => $o['AccountName'],
          'lname' => '',
          'order_id' => $o['DocNumber'],
          'phone' => $o['Phone'],
          // 'products' => $products
        ),
      );
      // var_dump($post_data);
      $orderKey = order_exists($o['DocNumber']);
      if ($orderKey !== false) {
        $post_data['ID'] = $orderKey;
      }
     $pid = wp_insert_post($post_data);
     if (!is_wp_error( $pid )) {
      update_field('products', $products, $pid);
      $i++;
     }
   	}
    echo json_encode(array('success' =>true, 'amount' => $i, 'newOffset' => $offset+$i));
    exit;
    
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
