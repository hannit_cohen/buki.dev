<?php
define('WP_USE_THEMES', false);
require('./wp-load.php');

$dsn = 'sqlsrv:Server=82.166.92.197;Database=lando07';
$user = 'sa';
$password = 'wizsoft';

try {
    $dbh = new PDO($dsn, $user, $password);

  // $stmt = $dbh->query("SELECT * FROM AccSortNames");
  // $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
  // var_dump($res);
  
  // echo "************\n"  ;

    $stmt = $dbh->query("SELECT ID,FullName,Address,City,Zip,Phone,SPhone,Email,AccountKey FROM Accounts where SortGroup = 200");
   	$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
   	$i = 0;
   	foreach ($res as $u) {
   		$i++;
   		if (!empty($u['Email'])) {
   			$email = $u['Email'];
   		} else {
   			$email = 'user'.$u['ID'].'@buki.dev';
   		}
      if (email_exists( $email ))
        continue;

      if (!empty($u['datF1'])){
        $pass = $u['datF1'];
      } else {
        $pass = $email;
      }

   		$user_data = array(
		    'user_login' =>  $email,
		    'display_name' => $u['FullName'],
        'first_name' => $u['FullName'],
		    'user_pass'  =>  $pass,
        'user_email' => $email,
   		);

   		$uid = wp_insert_user($user_data);
   		if (!is_wp_error( $uid )) {
        update_field('city', $u['City'], 'user_'.$uid);
        update_field('zip', $u['Zip'], 'user_'.$uid);
        update_field('address', $u['Address'], 'user_'.$uid);
        update_field('user_phone', $u['Phone'], 'user_'.$uid);
        update_field('user_phone2', $u['SPhone'], 'user_'.$uid);
        update_field('ID', $u['ID'], 'user_'.$uid);
   			update_field('AccountKey', $u['AccountKey'], 'user_'.$uid);


        // $q = "SELECT * FROM CRMContact where AccountKey = ".$u['AccountKey'];
        // $contact = $dbh->query($q);
        // $contact_res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // var_dump($contact_res);
        // echo "################";

        // if (!empty($contact_res)) {

        // }
   		}
   	}
    
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
